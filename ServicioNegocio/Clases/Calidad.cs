﻿using ServicioDatos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioNegocio.Clases
{
    public class Calidad
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public static List<Calidad> GetCalidades()
        {
            List<Calidad> calidades = new List<Calidad>();

            var bd = new ModeloFVEntities();

            try
            {
                foreach (var calidad in bd.CALIDAD)
                {
                    Calidad cali = new Calidad();
                    cali.Id = (int)calidad.ID;
                    cali.Nombre = calidad.NOMBRE;
                    calidades.Add(cali);
                }

            }
            catch (Exception)
            {

            }
            
            return calidades;

        }
    }
}
