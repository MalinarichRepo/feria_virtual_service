﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class DetalleOferta
    {

        public int IdPedido { get; set; }
        public int IdProducto { get; set; }
        public string RutProductor { get; set; }
        public int IdCalidad { get; set; }
        public int TotalOferta { get; set; }
        public int Toneladas { get; set; }
        public int Precio { get; set; }
        public int IdProductor { get; set; }
        public Productor Productor { get; set; }
        public List<DetalleEscogido> OfertasEscogidas { get; set; }
        public bool Escogida { get; set; }
        public bool Retirado { get; set; }

        public bool Ofertar()
        {
            var bd = new ModeloFVEntities();
            var oferta = new DE_OFERTA();

            try
            {
                oferta.PEDIDO_ID = this.IdPedido;
                oferta.PRODUCTO_ID = this.IdProducto;
                oferta.CALIDAD_ID = this.IdCalidad;
                oferta.PRODUCTOR_ID = this.IdProductor;
                oferta.TONELADAS = this.Toneladas;
                oferta.PRECIO = this.Precio;
                oferta.RETIRADO = "0";

                bd.DE_OFERTA.AddOrUpdate(oferta);
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }

        public static bool QuitarOferta(int idPedido, int idProducto, int idCalidad, int idProductor)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var oferta = bd.DE_OFERTA.Single(a => a.PEDIDO_ID == idPedido && a.PRODUCTO_ID == idProducto && a.CALIDAD_ID == idCalidad && a.PRODUCTOR_ID == idProductor);
                bd.DE_OFERTA.Remove(oferta);
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static int GetPrecioKilo(int idPedido, int idProducto, string rutProductor)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            int precio = 0;

            int precioKilo = (int)bd.DE_OFERTA.Single(a => a.PEDIDO_ID == idPedido && a.PRODUCTO_ID == idProducto && a.PRODUCTOR.USUARIO.RUT == rutProductor).PRECIO;

            foreach(var i in bd.DE_OFERTA)
            {
                if(i.PEDIDO_ID == idPedido && i.PRODUCTO_ID == idProducto && i.PRODUCTOR.USUARIO.RUT == rutProductor)
                {
                    precio = (int)i.PRECIO;
                    break;
                }
            }

            return precio;
        }

        public static List<DetalleOferta> GetOfertasPedidoProducto(int idPedido, int idProducto, int idCalidad)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<DetalleOferta>();

            try
            {
                var ofertas = bd.DE_OFERTA.Where(a => a.PEDIDO_ID == idPedido && a.PRODUCTO_ID == idProducto && a.CALIDAD_ID == idCalidad);

                foreach (var o in ofertas)
                {
                    var oferta = new DetalleOferta();
                    var productor = new Productor()
                    {
                        Id = (int)o.PRODUCTOR.USUARIO_ID,
                        Nombre = o.PRODUCTOR.USUARIO.NOMBRE,
                        Apellido = o.PRODUCTOR.USUARIO.APELLIDO
                    };

                    oferta.IdPedido = (int)o.PEDIDO_ID;
                    oferta.IdProducto = (int)o.PRODUCTO_ID;
                    oferta.IdCalidad = (int)o.CALIDAD_ID;
                    oferta.Precio = (int)o.PRECIO;
                    oferta.Toneladas = (int)o.TONELADAS;
                    oferta.TotalOferta = (int)(o.PRECIO * o.TONELADAS);

                    oferta.Productor = productor;

                    try
                    {
                        bd.DE_ESCOGIDO.Single(a => a.PEDIDO_ID == o.PEDIDO_ID && a.PRODUCTO_ID == o.PRODUCTO_ID && a.CALIDAD_ID == o.CALIDAD_ID && a.PRODUCTOR_ID == o.PRODUCTOR_ID);
                        oferta.Escogida = true;
                    }
                    catch (Exception)
                    {
                        oferta.Escogida = false;
                    }

                    lista.Add(oferta);

                }

                return lista;
            }
            catch (Exception)
            {
                return lista;
            }

        }

        public static bool EscogerOferta(int idPedido, int idProducto, int idCalidad, int idProductor, int toneladas)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var a = new DE_ESCOGIDO();

                a.PEDIDO_ID = idPedido;
                a.PRODUCTO_ID = idProducto;
                a.CALIDAD_ID = idCalidad;
                a.PRODUCTOR_ID = idProductor;
                a.TONELADAS = toneladas;

                bd.DE_ESCOGIDO.Add(a);
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
