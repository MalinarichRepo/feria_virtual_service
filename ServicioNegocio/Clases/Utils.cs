﻿using OracleInternal.Secure.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCrypto;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class Utils
    {
        
        public static bool ResetTokenExists(string resetToken)
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                bd.USUARIO.Single(a => a.RESET_TOKEN == resetToken);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal static int GetNextSubastaId()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var maxId = bd.SUBASTA.Max(table => table.ID);
                return (int)maxId + 1;
            }
            catch (Exception)
            {
                return 1;
            }
        }

        public static int GetNextUserId()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var maxId = bd.USUARIO.Max(table => table.ID);
                return (int)maxId + 1;
            }
            catch (Exception)
            {
                return 1;
            }
        }

        public static int GetNextPedidoId()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var maxId = bd.PEDIDO.Max(table => table.ID);
                return (int)maxId + 1;
            }
            catch (Exception)
            {
                return 1;
            }
        }

        public static int GetNextDireccionId()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var maxId = bd.DIRECCION.Max(table => table.ID);
                return (int)maxId + 1;
            }
            catch (Exception)
            {
                return 1;
            }
        }

    }
}
