﻿using ServicioDatos;
using SimpleCrypto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioNegocio.Clases
{
    public class Administrador : Usuario
    {
        public Usuario usuario { get; set; }


        public bool Registrar()
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            ADMINISTRADOR ad = new ADMINISTRADOR();
            USUARIO us = new USUARIO();

            us.ID = Utils.GetNextUserId();
            us.RUT = this.Rut;
            us.NOMBRE = this.Nombre;
            us.APELLIDO = this.Apellido;
            us.EMAIL = this.Email;

            ICryptoService cryptoService = new PBKDF2();
            cryptoService.HashIterations = 25000;
            string Password = cryptoService.Compute(this.Password);
            string Salt = cryptoService.Salt;

            us.PASSWORD = Password;
            us.SALT = Salt;
            us.TIPO_USUARIO = 4;
            us.TOKEN = "0";
            us.TELEFONO = this.Telefono;
            us.RESET_TOKEN = "";


            ad.USUARIO = us;


            /*bd.USUARIO.Add(us);*/
            
            bd.ADMINISTRADOR.Add(ad);
            bd.SaveChanges();

            return true;

        }
        public static Administrador Get_Administrador_By_Id(int id)
        {
            Administrador admin = new Administrador();
            Usuario usuario = new Usuario();
            ModeloFVEntities bd = new ModeloFVEntities();

            var found = bd.ADMINISTRADOR.Find(id);

            usuario.Rut = found.USUARIO.RUT;
            usuario.Nombre = found.USUARIO.NOMBRE;
            usuario.Apellido = found.USUARIO.APELLIDO;

            admin.usuario = usuario;

            return admin;


        }

        public static List<Administrador> GetAllAdmins()
        {
            List<Administrador> lista = new List<Administrador>();
            ModeloFVEntities bd = new ModeloFVEntities();

            foreach(var found in bd.ADMINISTRADOR)
            {
                Administrador admin = new Administrador();
                Usuario usuario = new Usuario();
                usuario.Rut = found.USUARIO.RUT;
                usuario.Nombre = found.USUARIO.NOMBRE;
                usuario.Apellido = found.USUARIO.APELLIDO;
                admin.usuario = usuario;

                lista.Add(admin);

            }

            return lista;


        }

    }
}
