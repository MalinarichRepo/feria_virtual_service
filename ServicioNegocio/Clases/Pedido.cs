﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class Pedido
    {

        public int Id { get; set; }
        public string Fecha { get; set; }
        public Cliente Cliente { get; set; }
        public Estado Estado { get; set; }
        public Direccion Direccion { get; set; }
        public int Total { get; set; }
        public List<DetallePedido> DetallePedido { get; set; }
        public bool Completo { get; set; }
        public Transportista Transportista { get; set; }
        public DetalleTransporte DetalleTransporte { get; set; }

        public Pedido() { }

        public Pedido(int Id, string Fecha, Estado Estado, Direccion Direccion, int Total)
        {
            this.Id = Id;
            this.Fecha = Fecha;
            this.Estado = Estado;
            this.Direccion = Direccion;
            this.Total = Total;
        }

        public static bool HabilitarPago(int idPedido)
        {
            var bd = new ModeloFVEntities();
            try
            {
                bd.PEDIDO.Find(idPedido).ESTADO_ID = 10;
                bd.SaveChanges();

                var pedido = bd.PEDIDO.Find(idPedido);
                
                var mail = new Mail();
                mail.Subject = $"Pago habilitado para pedido #{idPedido}.";
                mail.To = pedido.CLIENTE.USUARIO.EMAIL;
                mail.Body = $"<span class='text-bold'>Estimado/a {pedido.CLIENTE.USUARIO.NOMBRE} {pedido.CLIENTE.USUARIO.APELLIDO}</span><br><br>Te informamos que el pago para tu pedido #{idPedido} ha sido habilitado.<br>Puedes realizar el pago en la sección de pedidos o hacer click <a href='#'>aquí</a> para ir directamente al pedido.";


                mail.Send();


                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RealizarPago(int idPedido)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);
                pedido.ESTADO_ID = 11;
                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ConfirmarRecepcion(int idPedido)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);
                pedido.ESTADO_ID = 9;
                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ConfirmarEntrega(int idPedido)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);
                pedido.ESTADO_ID = 8;
                bd.SaveChanges();

                var mail = new Mail();
                mail.Subject = $"Se requiere tu confirmación para pedido #{idPedido}.";
                mail.To = pedido.CLIENTE.USUARIO.EMAIL;
                mail.Body = $"<span class='text-bold'>Estimado/a {pedido.CLIENTE.USUARIO.NOMBRE} {pedido.CLIENTE.USUARIO.APELLIDO}</span><br><br>Te informamos que el transportista de tu pedido #{idPedido} ha marcado el pedido como entregado.<br>Se requiere tu confirmación de recepción del pedido.<br>Puedes acceder desde nuestro sitio web, en la sección de pedidos, o puedes acceder directamente al pedido haciendo click <a href='#'>aquí</a>.";
                mail.Send();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ConfirmarRetiro(int idPedido)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);
                pedido.ESTADO_ID = 7;
                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static bool CancelarPedido(int idPedido)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);

                if (pedido.ESTADO_ID <= 5)
                {
                    if (pedido.ESTADO_ID == 5)
                    {
                        var subasta = bd.SUBASTA.Single(a => a.PEDIDO_ID == idPedido);
                        subasta.ESTADO_ID = 25;
                    }

                    pedido.ESTADO_ID = 12;

                    bd.SaveChanges();
                    return true;

                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool ConfirmarPedido(int idPedido)
        {
            var bd = new ModeloFVEntities();
            var pedido = bd.PEDIDO.Find(idPedido);

            try
            {
                pedido.ESTADO_ID = 5;
                var sub = new SUBASTA();
                sub.ID = Utils.GetNextSubastaId();
                sub.PEDIDO_ID = idPedido;
                sub.ESTADO_ID = 21;
                sub.FECHA_INICIO = DateTime.Now;
                sub.FECHA_TERMINO = DateTime.Now.AddDays(3);

                bd.SUBASTA.Add(sub);
                bd.SaveChanges();

                // Notificar a los transportistas
                //foreach (var transportista in bd.TRANSPORTISTA)
                //{
                //    var email = new Mail();
                //    email.Subject = "Nuevo subasta para pedido #" + idPedido;
                //    email.Body = "Hola " + transportista.USUARIO.EMAIL + ", existe una nueva subasta sobre la cual puedes participar.";
                //    email.Send();
                //}



                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static bool CompletarPedido(int idPedido)
        {
            var bd = new ModeloFVEntities();
            var pedido = bd.PEDIDO.Find(idPedido);

            var productoresANotificar = new List<int>();

            try
            {
                pedido.ESTADO_ID = 4;
                bd.SaveChanges();

                // Notificar a productores cuyas ofertas fueran escogidas
                var OfertasEscogidas = bd.DE_ESCOGIDO.Where(a => a.PEDIDO_ID == idPedido);

                foreach (var oferta in OfertasEscogidas)
                {
                    //var correo = bd.PRODUCTOR.Find(oferta.PRODUCTOR_ID).USUARIO.EMAIL;
                    // Solo agregar productor si es que no se encuentra en la lista, para evitar duplicados
                    if (!productoresANotificar.Contains((int)oferta.PRODUCTOR_ID))
                        productoresANotificar.Add((int)oferta.PRODUCTOR_ID);
                }

                // Notificar a los productores
                foreach (var i in productoresANotificar)
                {
                    var email = new Mail();
                    var productor = bd.PRODUCTOR.Find(i).USUARIO;
                    email.Subject = $"Tus ofertas han sido escogidas para el pedido #{idPedido}";
                    email.To = productor.EMAIL;
                    email.Body = $"<span class='text-bold'>Estimado/a {productor.NOMBRE} {productor.APELLIDO}</span><br><br>Te informamos que una o mas de tus ofertas han sido escogidas para el pedido #{idPedido}.<br>Te mantendremos informado/a sobre el estado de este pedido.";

                    email.Send();
                }

                // Notificar al cliente
                var mail = new Mail();
                var cliente = pedido.CLIENTE.USUARIO;
                mail.Subject = $"Las ofertas para tu pedido #{idPedido} han sido escogidas y requerimos tu confirmación.";
                mail.To = pedido.CLIENTE.USUARIO.EMAIL;
                mail.Body = "Las ofertas para tu pedido #" + idPedido + " han sido escogidas y se requiere confirmación.";
                mail.Body = $"<span class='text-bold'>Estimado/a {cliente.NOMBRE} {cliente.APELLIDO}</span><br><br>Te informamos que las ofertas para tu pedido #{idPedido} han sido escogidas.<br>Se requiere confirmación de tu parte para saber que estas conforme con las ofertas.<br>Puedes acceder a tu pedido en la sección de pedidos o haciendo click <a href='#'>aquí</a>.";

                mail.Send();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static List<Pedido> GetPedidosEnSeleccion()
        {
            var lista = new List<Pedido>();
            var bd = new ModeloFVEntities();

            try
            {
                var pedidosEnSeleccion = bd.PEDIDO.Where(a => a.ESTADO.ID >= 1 && a.ESTADO_ID <= 10);

                foreach (var p in pedidosEnSeleccion)
                {
                    var pedido = new Pedido() {
                        Id = (int)p.ID,
                        Estado = Estado.GetEstado((int)p.ESTADO.ID),
                        Fecha = p.FECHA.ToString("dd/MM/yyyy"),
                        Cliente = Cliente.GetClienteById((int)p.CLIENTE_ID),
                        Direccion = Direccion.GetDireccionById((int)p.DIRECCION_ID)
                    };
                    lista.Add(pedido);
                }

                return lista;
            }
            catch (Exception)
            {
                return lista;
            }

        }

        public static string GetInvitados(int idPedido)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Productor>();

            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);
                foreach (var invitado in pedido.PRODUCTOR)
                {
                    lista.Add(new Productor()
                    {
                        Id = (int)invitado.USUARIO_ID,
                        Nombre = invitado.USUARIO.NOMBRE,
                        Apellido = invitado.USUARIO.APELLIDO,
                        Email = invitado.USUARIO.EMAIL,
                        Telefono = (int)invitado.USUARIO.TELEFONO
                    });
                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
        }

        public bool CrearPedido()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            PEDIDO ped = new PEDIDO();
            ped.ID = Utils.GetNextPedidoId();
            ped.FECHA = DateTime.Today;
            ped.ESTADO_ID = 1;
            ped.DIRECCION = bd.DIRECCION.Find(this.Direccion.Id);
            ped.CLIENTE_ID = this.Cliente.Id;

            var correosANotificar = new List<string>();

            // Por cada producto en el pedido
            foreach (var i in this.DetallePedido)
            {
                DE_PEDIDO p = new DE_PEDIDO();
                p.PEDIDO_ID = ped.ID;
                p.PRODUCTO_ID = i.Producto.Id;
                p.CALIDAD_ID = i.Calidad.Id;
                p.TONELADAS = i.Toneladas;
                ped.DE_PEDIDO.Add(p);

                //// Traer lista de productores que ofrecen este producto
                //foreach (var prodOfrecido in bd.PROD_OFRECIDOS.Where(a => a.PRODUCTO_ID == i.Producto.Id && a.HABILITADO == "1" ))
                //{
                //    var correo = prodOfrecido.PRODUCTOR.USUARIO.EMAIL;
                //    // Solo agregar correo si es que no se encuentra en la lista, para evitar duplicados
                //    if (!correosANotificar.Contains(correo))
                //        correosANotificar.Add(correo);
                //}

            }

            bd.PEDIDO.Add(ped);
            bd.SaveChanges();

            //foreach (var i in correosANotificar)
            //{
            //    var email = new Mail();
            //    email.Subject = "Nuevo pedido (#"+ ped.ID +") con productos que ofreces.";
            //    email.Body = "Hola " + i + ", Existe un nuevo pedido el cual requiere de productos los cuales tu ofreces.";
            //    email.Send();
            //}

            return true;
        }

        public static List<Pedido> GetPedidosFinalizados(int id_usuario)
        {

            ModeloFVEntities bd = new ModeloFVEntities();
            List<Pedido> lista = new List<Pedido>();

            var cliente = bd.CLIENTE.Find(id_usuario);

            foreach (var p in cliente.PEDIDO.Where(a => a.ESTADO_ID == 11 || a.ESTADO_ID == 12))
            {
                Estado estado = new Estado { Id = (int)p.ESTADO.ID, Nombre = p.ESTADO.NOMBRE, Descripcion = p.ESTADO.DESCRIPCION };
                Pedido pedido = new Pedido((int)p.ID, p.FECHA.ToString("dd/MM/yyyy"), estado, Direccion.GetDireccionById((int)p.DIRECCION_ID), GetTotal((int)p.ID));
                lista.Add(pedido);

            }
            return lista;

        }

        public static List<Pedido> GetPedidosActivos(int id_usuario)
        {

            ModeloFVEntities bd = new ModeloFVEntities();
            List<Pedido> lista = new List<Pedido>();

            var cliente = bd.CLIENTE.Find(id_usuario);

            foreach (var p in cliente.PEDIDO.Where(a => a.ESTADO_ID >= 1 && a.ESTADO_ID <= 10))
            {
                Estado estado = new Estado { Id = (int)p.ESTADO.ID, Nombre = p.ESTADO.NOMBRE, Descripcion = p.ESTADO.DESCRIPCION };
                Pedido pedido = new Pedido((int)p.ID, p.FECHA.ToString("dd/MM/yyyy"), estado, Direccion.GetDireccionById((int)p.DIRECCION_ID), GetTotal((int)p.ID));
                lista.Add(pedido);

            }
            return lista;

        }
        public static List<Pedido> GetPedidos(int id_usuario)
        {

            ModeloFVEntities bd = new ModeloFVEntities();
            List<Pedido> lista = new List<Pedido>();

            var cliente = bd.CLIENTE.Find(id_usuario);

            foreach (var p in cliente.PEDIDO)
            {
                Estado estado = new Estado { Id = (int)p.ESTADO.ID, Nombre = p.ESTADO.NOMBRE, Descripcion = p.ESTADO.DESCRIPCION };
                Pedido pedido = new Pedido((int)p.ID, p.FECHA.ToString("dd/MM/yyyy"), estado, Direccion.GetDireccionById((int)p.DIRECCION_ID), GetTotal((int)p.ID));
                lista.Add(pedido);

            }
            return lista;

        }

        public static List<Pedido> GetAllPedidos()
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            List<Pedido> lista = new List<Pedido>();

            foreach (var p in bd.PEDIDO)
            {
                Estado estado = new Estado { Id = (int)p.ESTADO.ID, Nombre = p.ESTADO.NOMBRE, Descripcion = p.ESTADO.DESCRIPCION };
                Pedido pedido = new Pedido((int)p.ID, p.FECHA.ToString("dd/MM/yyyy"), estado, Direccion.GetDireccionById((int)p.DIRECCION_ID), GetTotal((int)p.ID));
                lista.Add(pedido);
            }

            return lista;

        }

        public static Pedido GetPedido(int idPedido)
        {
            var bd = new ModeloFVEntities();
            var p = bd.PEDIDO.Find(idPedido);
            Cliente cliente = new Cliente();

            var pedido = new Pedido()
            {
                Id = (int)p.ID,
                Fecha = p.FECHA.ToString("dd/MM/yyyy"),
                Estado = new Estado() { Id = (int)p.ESTADO.ID, Nombre = p.ESTADO.NOMBRE, Descripcion = p.ESTADO.DESCRIPCION },
                Direccion = Direccion.GetDireccionById((int)p.DIRECCION.ID),
                Cliente = Cliente.GetClienteById((int)p.CLIENTE.USUARIO_ID),
                Total = GetTotal(idPedido),
                DetallePedido = Clases.DetallePedido.GetDetallePedido(idPedido)
            };

            // Transportista asignado (Si es que ya se cumplio el proceso de subasta)
            try
            {
                var subasta = bd.SUBASTA.Single(a => a.PEDIDO_ID == idPedido);
                // var valorOfertaGanadora = subasta.BID.Min(a => a.PRECIO);
                // var ofertaGanadora = bd.BID.Single(a => a.PRECIO == valorOfertaGanadora);
                var ofertaGanadora = bd.BID.Single(a => a.SUBASTA_ID == subasta.ID && a.ESCOGIDA == "1");
                var transportistaGanador = bd.TRANSPORTISTA.Find(ofertaGanadora.TRANSPORTISTA_ID);

                pedido.Transportista = new Transportista()
                {
                    Id = (int)transportistaGanador.USUARIO_ID,
                    Nombre = transportistaGanador.USUARIO.NOMBRE,
                    Apellido = transportistaGanador.USUARIO.APELLIDO,
                    Telefono = (int)transportistaGanador.USUARIO.TELEFONO,
                    Email = transportistaGanador.USUARIO.EMAIL
                };

                //pedido.DetallePedido.Add(new DetallePedido() { Producto = new Producto() { Nombre = $"Despacho: {transportistaGanador.USUARIO.NOMBRE} {transportistaGanador.USUARIO.APELLIDO}" }, Ofertas = new List<DetalleOferta>() { new DetalleOferta() { Escogida = true, TotalOferta = (int)ofertaGanadora.PRECIO} } });
                pedido.DetalleTransporte = new DetalleTransporte() { Transportista = $"Despacho: {transportistaGanador.USUARIO.NOMBRE} {transportistaGanador.USUARIO.APELLIDO}", Subtotal = (int)ofertaGanadora.PRECIO };
                pedido.Total += (int)ofertaGanadora.PRECIO;
            }
            catch (Exception)
            {

            }


            var countPedido = bd.DE_PEDIDO.Where(a => a.PEDIDO_ID == idPedido).Count();
            var countEscogido = bd.DE_ESCOGIDO.Where(a => a.PEDIDO_ID == idPedido).Count();

            if (countPedido == countEscogido)
            {
                pedido.Completo = true;
            }

            return pedido;

        }

        public static Pedido GetPedido(int idPedido, int idProductor)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            var p = bd.PEDIDO.Find(idPedido);

            var pedido = new Pedido()
            {
                Id = (int)p.ID,
                Fecha = p.FECHA.ToString("dd/MM/yyyy"),
                Estado = new Estado() { Id = (int)p.ESTADO.ID, Nombre = p.ESTADO.NOMBRE, Descripcion = p.ESTADO.DESCRIPCION },
                Direccion = Direccion.GetDireccionById((int)p.DIRECCION.ID),
                Cliente = Cliente.GetClienteById((int)p.CLIENTE.USUARIO_ID),
                Total = GetTotal(idPedido)
            };

            pedido.DetallePedido = Clases.DetallePedido.GetDetallePedido(idPedido, idProductor);

            return pedido;

        }

        public static int GetTotal(int idPedido)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            int total = 0;

            var escogidos = bd.DE_ESCOGIDO.Where(a => a.PEDIDO_ID == idPedido);

            try
            {

                foreach (var i in escogidos)
                {
                    var oferta = bd.DE_OFERTA.Single(a => a.PEDIDO_ID == i.PEDIDO_ID && a.PRODUCTOR_ID == i.PRODUCTOR_ID && a.PRODUCTO_ID == i.PRODUCTO_ID && a.CALIDAD_ID == i.CALIDAD_ID);
                    total += (int)oferta.PRECIO * (int)oferta.TONELADAS;
                }

                return total;

            }
            catch (Exception)
            {
                return total;
            }

        }

        public static bool Invitar(int idPedido, List<Productor> invitados)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);

                foreach (var invitado in invitados)
                {
                    var productor = bd.PRODUCTOR.Find(invitado.Id);
                    pedido.PRODUCTOR.Add(productor);

                    // Notificar por correo
                    Mail mail = new Mail();
                    mail.Subject = $"Has sido invitado a participar en el pedido #{idPedido}!";
                    mail.To = bd.USUARIO.Find(invitado.Id).EMAIL;
                    mail.Body = $"<span class='text-bold'>Estimado/a {productor.USUARIO.NOMBRE} {productor.USUARIO.APELLIDO}</span><br><br>Te informamos que has sido invitado a participar en el pedido #{idPedido}.<br>Este se encontrara disponible en tu sección de pedidos pendientes, o puedes acceder directamente haciendo click <a href='#'>aquí</a>.";

                    mail.Send();

                }

                pedido.ESTADO_ID = 2;

                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string GetPosiblesProductores(int idPedido)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Productor>();
            
            try
            {
                var pedido = bd.PEDIDO.Find(idPedido);

                foreach (var i in pedido.DE_PEDIDO)
                {
                    var abc = bd.PROD_OFRECIDOS.Where(a => a.PRODUCTO_ID == i.PRODUCTO_ID && a.CALIDAD_ID == i.CALIDAD_ID && a.HABILITADO == "1");

                    foreach (var producto in abc)
                    {
                        Productor productor = new Productor() { Id = (int)producto.PRODUCTOR.USUARIO_ID, Nombre = producto.PRODUCTOR.USUARIO.NOMBRE, Apellido = producto.PRODUCTOR.USUARIO.APELLIDO, Email = producto.PRODUCTOR.USUARIO.EMAIL, Telefono = (int)producto.PRODUCTOR.USUARIO.TELEFONO };

                        productor.ProductosOfrecidos = Productor.GetProductosOfrecidos(productor.Id);
                        


                        if (!lista.Any(a => a.Id == productor.Id)) // Solo agregar a la lista si es que no se encuentra ya
                        {
                            lista.Add(productor);
                        }

                    }

                }

                return JsonConvert.SerializeObject(lista);

            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
            

        }

    }
}
