﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class ProductoOfrecido
    {

        public Producto Producto { get; set; }
        public Productor Productor { get; set; }
        public Calidad Calidad { get; set; }
        public List<Temporada> Temporadas { get; set; }
        public string Habilitado { get; set; }

        public static bool ToggleHabilitado(int productoId, int productorId, int calidadId)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var prod_ofrecido = bd.PROD_OFRECIDOS.Single(a => a.PRODUCTO.ID == productoId && a.PRODUCTOR.USUARIO.ID == productorId && a.CALIDAD.ID == calidadId);

                if (prod_ofrecido.HABILITADO == "1")
                    prod_ofrecido.HABILITADO = "0";
                else
                    prod_ofrecido.HABILITADO = "1";

                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



    }
}
