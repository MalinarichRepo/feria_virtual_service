﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioNegocio.Clases
{
    public class TokenMessage
    {
        public string Token { get; set; }
        public string Message { get; set; }
        public int TipoUsuario { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

    }
}
