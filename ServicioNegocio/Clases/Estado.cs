﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class Estado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public static Estado GetEstado(int idEstado)
        {
            var bd = new ModeloFVEntities();
            var estado = new Estado();

            try
            {
                var a = bd.ESTADO.Find(idEstado);

                estado.Id = (int)a.ID;
                estado.Nombre = a.NOMBRE;
                estado.Descripcion = a.DESCRIPCION;

                return estado;
            }
            catch (Exception)
            {
                return estado;
            }

        }


    }
}
