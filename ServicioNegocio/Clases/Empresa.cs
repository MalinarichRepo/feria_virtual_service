﻿using ServicioDatos;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Configuration;
using System.Data;
using System.Data.SqlTypes;

namespace ServicioNegocio.Clases
{
    public class Empresa
    {
        public int Id { get; set; }
        public string NombreEmpresa { get; set; }


        public static List<Empresa> GetEmpresas()
        {
            List<Empresa> listemp = new List<Empresa>();
            string connectionString = ConfigurationManager.ConnectionStrings["OracleFunction"].ConnectionString;
            OracleConnection conexionora = new OracleConnection(connectionString);

            try
            {
                conexionora.Open();

                

            }
            catch (Exception ex)
            {

                throw ex;
            }

            try
            {
                
                OracleCommand cmd = new OracleCommand("FN_GETEMPRESAS", conexionora);

                cmd.CommandType = CommandType.StoredProcedure;

              

                OracleParameter output = cmd.Parameters.Add("L_CURSOR", OracleDbType.RefCursor);

                output.Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();

                OracleDataReader reader = ((OracleRefCursor)output.Value).GetDataReader();

                

                while (reader.Read())
                {
                    Empresa emp = new Empresa();

                    emp.Id = reader.GetInt32(0);
                    emp.NombreEmpresa = reader.GetString(1);

                    listemp.Add(emp);
                }

                

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return listemp;
        }
    }
}
