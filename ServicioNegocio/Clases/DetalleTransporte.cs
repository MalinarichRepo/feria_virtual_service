﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class DetalleTransporte
    {

        public string Transportista { get; set; }
        public int Subtotal { get; set; }

    }
}
