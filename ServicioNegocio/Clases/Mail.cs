﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Web;
using System.Net.Mime;

namespace ServicioNegocio.Clases
{
    public class Mail
    {

        public static string From { get; set; } = "smtptest675@gmail.com";
        public static NetworkCredential Credentials { get; set; } = new NetworkCredential("smtptest675@gmail.com", "123456789asd_");
        public static string Host { get; set; } = "smtp.gmail.com";
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public static bool Enabled { get; set; } = true;
        public static bool Debug { get; set; } = true; // If set to true, it will send all mails to the specified debug email
        public static string DebugTo { get; set; } = "smtptest675@gmail.com";

        public Mail()
        {
            //this.From = "feriavirtualportafolio2020@hotmail.com";
            //this.Credentials = new NetworkCredential("feriavirtualportafolio2020@hotmail.com", "duoc2020");
        }

        public bool Send()
        {
            if (Mail.Enabled)
            {
                try
                {
                    MailMessage msg = new MailMessage();

                    if (Mail.Debug)
                        msg.To.Add(Mail.DebugTo);
                    else
                        msg.To.Add(this.To);

                    msg.Subject = this.Subject;
                    msg.SubjectEncoding = System.Text.Encoding.UTF8;

                    //msg.Body = this.Body;
                    //msg.BodyEncoding = System.Text.Encoding.UTF8;

                    string html = @"<html>
    <head>
        <style>
            body{
                font-family: Segoe UI, Tahoma, Geneva, Verdana, sans-serif;
            }
            h2{
                font-weight: normal;
            }
            #logo{
                max-width:160px;
            }
            hr{
                border: none;height: 1px;background-color: rgba(0, 0, 0, 0.1);
            }
            .text-bold{
                font-weight: bold;
            }
        table, th, td
        {
            border: 1px solid rgb(185, 185, 185);
            padding: 5px;
        }
        table{
            border-collapse: collapse;
            text-align: center;
        }
        </style>
    </head>
<body>
    <div style='width: 100%;margin:auto;'>
        <div style='width: 100%;margin:auto;'>
            <img src='cid:logo' id='logo'>
            <hr><br>" + this.Body + @"<br><br>
<hr><br>
<span class='text-bold'>CONTACTO</span>
<br>
            
            Av.Los Queñes 35.780, Barrio Industrial, Quilicura -Santiago
           <br>
           Mesa Central: +56 2 2990 3004
           <br>
           contacto@maipogrande.cl
       </div>
    </div>
</body>

</html>";
                    AlternateView altView = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
                    string logoPath = $@"{AppDomain.CurrentDomain.BaseDirectory}..\ServicioNegocio\Images\feriavirtual4x.png";
                    LinkedResource pictureRes = new LinkedResource(logoPath, MediaTypeNames.Image.Jpeg);
                    pictureRes.ContentId = "logo";
                    altView.LinkedResources.Add(pictureRes);

                    msg.AlternateViews.Add(altView);

                    // ===================
                    msg.IsBodyHtml = true;
                    // ===================

                    msg.From = new MailAddress(Mail.From);
                    SmtpClient cliente = new SmtpClient
                    {
                        Credentials = Mail.Credentials,
                        Port = 587,
                        EnableSsl = true,
                        Host = Mail.Host
                    };

                    cliente.Send(msg);

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public string EnviarContrasenia(Transportista transportista)
        {

            MailMessage msg = new MailMessage();

            if (Mail.Debug)
                msg.To.Add(Mail.DebugTo);
            else
                msg.To.Add(this.To);

            msg.Subject = this.Subject;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;

            string password = GeneradorToken.GenerarContrasena();


            msg.BodyEncoding = System.Text.Encoding.UTF8;

            msg.IsBodyHtml = true;

            try
            {
                string html = @"<html>
            <head>
                <style>
                    body{
                        font-family: Segoe UI, Tahoma, Geneva, Verdana, sans-serif;
                    }
                    h2{
                        font-weight: normal;
                    }
                    #logo{
                        max-width:160px;
                    }
                    hr{
                        border: none;height: 1px;background-color: rgba(0, 0, 0, 0.1);
                    }
                    .text-bold{
                        font-weight: bold;
                    }
                table, th, td
                {
                    border: 1px solid rgb(185, 185, 185);
                    padding: 5px;
                }
                table{
                    border-collapse: collapse;
                    text-align: center;
                }
                </style>
            </head>
        <body>
            <div style='width: 100%;margin:auto;'>
                <div style='width: 100%;margin:auto;'>
                    <img src='cid:logo' id='logo'>
                    <hr><br>" + $"<span class='text-bold'>Estimado/a {transportista.Nombre} {transportista.Apellido}</span><br><br>Te informamos que tu cuenta ha sido creada exitosamente, a continuación se incluye tu contraseña provisoria,<br>la cual deberas cambiar en un maximo de 24 horas una vez hayas iniciado sesión por primera vez.<br><br>Email: {transportista.Email}<br>Contraseña: {password}" + @"<br><br>
        <hr><br>
        <span class='text-bold'>CONTACTO</span>
        <br>
            
                    Av.Los Queñes 35.780, Barrio Industrial, Quilicura -Santiago
                   <br>
                   Mesa Central: +56 2 2990 3004
                   <br>
                   contacto@maipogrande.cl
               </div>
            </div>
        </body>

        </html>";
                AlternateView altView = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
                string startupPath = System.IO.Directory.GetCurrentDirectory();

                string logoPath = $@"{AppDomain.CurrentDomain.BaseDirectory}..\ServicioNegocio\Images\feriavirtual4x.png";
                LinkedResource pictureRes = new LinkedResource(logoPath, MediaTypeNames.Image.Jpeg);
                pictureRes.ContentId = "logo";
                altView.LinkedResources.Add(pictureRes);

                msg.AlternateViews.Add(altView);

                // ===================
                msg.IsBodyHtml = true;
                // ===================

                msg.From = new MailAddress(Mail.From);
                SmtpClient cliente = new SmtpClient
                {
                    Credentials = Mail.Credentials,
                    Port = 587,
                    EnableSsl = true,
                    Host = Mail.Host
                };

                cliente.Send(msg);
                return password;

            }
            catch (Exception)
            {
                return password;
            }

        }



    }
}
