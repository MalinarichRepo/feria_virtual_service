﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioNegocio.Clases
{
    public class Body
    {
        public List<Producto> Productos { get; set; }
        public string Email { get; set; }
    }
}
