﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using SimpleCrypto;

namespace ServicioNegocio.Clases
{
    public class Cliente : Usuario
    {
        public bool Registrar()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            ICryptoService cryptoService = new PBKDF2();
            cryptoService.HashIterations = 25000;
            string Password = cryptoService.Compute(this.Password);
            string Salt = cryptoService.Salt;

            try
            {
                USUARIO us = new USUARIO
                {
                    ID = Utils.GetNextUserId(),
                    RUT = this.Rut,
                    NOMBRE = this.Nombre,
                    APELLIDO = this.Apellido,
                    EMAIL = this.Email,
                    PASSWORD = Password,
                    SALT = Salt,
                    TIPO_USUARIO = 1,
                    TOKEN = "",
                    TELEFONO = this.Telefono,
                    RESET_TOKEN = ""
                };

                CLIENTE cli = new CLIENTE
                {
                    USUARIO = us
                };

                bd.CLIENTE.Add(cli);
                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static Cliente GetClienteById(int idCliente)
        {
            var bd = new ModeloFVEntities();
            var cli = bd.CLIENTE.Find(idCliente);

            Cliente cliente = new Cliente()
            {
                Id = (int)cli.USUARIO_ID,
                Rut = cli.USUARIO.RUT,
                Nombre = cli.USUARIO.NOMBRE,
                Apellido = cli.USUARIO.APELLIDO,
                Email = cli.USUARIO.EMAIL,
                TipoUsuario = (int)cli.USUARIO.TIPO_USUARIO
            };

            return cliente;

        }
    }
}
