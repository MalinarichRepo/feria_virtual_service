﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Types;
using ServicioDatos;
using SimpleCrypto;
using Newtonsoft.Json;

namespace ServicioNegocio.Clases
{
    public class Productor : Usuario
    {
        public DateTime expiracion_contrato { get; set; }
        public List<ProductoOfrecido> ProductosOfrecidos { get; set; }

        public bool Registrar()
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            PRODUCTOR pro = new PRODUCTOR();
            USUARIO us = new USUARIO();

            us.ID = Utils.GetNextUserId();
            us.RUT = this.Rut;
            us.NOMBRE = this.Nombre;
            us.APELLIDO = this.Apellido;
            us.EMAIL = this.Email;

            ICryptoService cryptoService = new PBKDF2();
            cryptoService.HashIterations = 25000;
            string Password = cryptoService.Compute(this.Password);
            string Salt = cryptoService.Salt;

            us.PASSWORD = Password;
            us.SALT = Salt;
            us.TIPO_USUARIO = 2;
            us.TOKEN = "0";
            us.TELEFONO = this.Telefono;
            us.RESET_TOKEN = "";
            pro.EXPIRACION_CONTRATO = this.expiracion_contrato;
            pro.USUARIO = us;

            bd.PRODUCTOR.Add(pro);
            bd.SaveChanges();

            return true;

        }

        public static List<ProductoOfrecido> GetProductosOfrecidos(int idProductor)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            List<ProductoOfrecido> lista = new List<ProductoOfrecido>();

            var productor = bd.PRODUCTOR.Find(idProductor);

            foreach (var producto in productor.PROD_OFRECIDOS)
            {
                var temporadas = new List<Temporada>();

                foreach (var temp in producto.PRODUCTO.TEMPORADA)
                {
                    var temporada = new Temporada()
                    {
                        Id = (int)temp.ID,
                        Nombre = temp.NOMBRE
                    };

                    temporadas.Add(temporada);
                }

                var p = new ProductoOfrecido
                {
                    Producto = new Producto { Id = (int)producto.PRODUCTO_ID, Nombre = producto.PRODUCTO.NOMBRE, Calidad = new Calidad { Id = (int)producto.CALIDAD.ID, Nombre = producto.CALIDAD.NOMBRE } },
                    Temporadas = temporadas,
                    Calidad = new Calidad { Id = (int)producto.CALIDAD.ID, Nombre = producto.CALIDAD.NOMBRE },
                    Habilitado = producto.HABILITADO
                };

                lista.Add(p);
            }

            return lista;

        }
        public static List<Producto> GetProductosNoOfrecidos(int idProductor)
        {
            var todos = Producto.GetProductos();

            var ofrecidos = Productor.GetProductosOfrecidos(idProductor);

            //var noOfrecidos = todos.Intersect(ofrecidos).ToList();
            var noOfrecidos = todos.Where(el2 => !ofrecidos.Any(el1 => el1.Producto.Id == el2.Id)).ToList();
            return noOfrecidos;

        }

        public static bool AddProductoOfrecido(int idProductor, int idProducto, int idCalidad)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            try
            {
                //var productor = bd.PRODUCTOR.Find(idProductor);
                //var producto = bd.PRODUCTO.Find(idProducto);
                var prod_ofrecido = new PROD_OFRECIDOS { PRODUCTO_ID = idProducto, PRODUCTOR_ID = idProductor, CALIDAD_ID = idCalidad };
                prod_ofrecido.HABILITADO = "1";
                bd.PROD_OFRECIDOS.Add(prod_ofrecido);
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemoveProductoOfrecido(int idProductor, int idProducto, int idCalidad)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            try
            {
                //var productor = bd.PRODUCTOR.Find(idProductor);
                //var producto = bd.PRODUCTO.Find(idProducto);

                //productor.PRODUCTO.Remove(producto);
                bd.PROD_OFRECIDOS.Remove(bd.PROD_OFRECIDOS.Single(a => a.PRODUCTO.ID == idProducto && a.PRODUCTOR.USUARIO.ID == idProductor && a.CALIDAD.ID == idCalidad));

                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string PedidosOfertados(int idProductor)
        {
            var lista = new List<Pedido>();
            var bd = new ModeloFVEntities();

            try
            {
                // Lista con el detalle de los productos que se han ofertado
                // Distinct para traer cada pedido solo una vez
                var detOfertas = bd.DE_OFERTA.Where(a => a.PRODUCTOR_ID == idProductor).Select(a => new { a.PEDIDO_ID }).Distinct();

                foreach (var det in detOfertas)
                {
                    var i = bd.PEDIDO.Find(det.PEDIDO_ID);
                    // Comprobar que el estado del pedido no es entregado o cancelado
                    if (i.ESTADO.ID >= 2 && i.ESTADO.ID <= 7)
                    {
                        Estado estado = new Estado { Id = (int)i.ESTADO.ID, Nombre = i.ESTADO.NOMBRE, Descripcion = i.ESTADO.DESCRIPCION };
                        Pedido pedido = new Pedido((int)i.ID, i.FECHA.ToString("dd/MM/yyyy"), estado, Direccion.GetDireccionById((int)i.DIRECCION_ID), Pedido.GetTotal((int)i.ID));
                        lista.Add(pedido);
                    }
                }

                return JsonConvert.SerializeObject(lista);

            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }

        }


        public static string PedidosFinalizados(int idProductor)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Pedido>();

            try
            {
                // Lista con el detalle de los productos que se han ofertado
                // Distinct para traer cada pedido solo una vez
                var detOfertas = bd.DE_OFERTA.Where(a => a.PRODUCTOR_ID == idProductor).Select(a => new { a.PEDIDO_ID }).Distinct();

                foreach (var det in detOfertas)
                {
                    var i = bd.PEDIDO.Find(det.PEDIDO_ID);
                    // Comprobar que el estado del pedido ha pasado de la etapa de selección y confirmación por parte del cliente
                    if (i.ESTADO.ID > 4)
                    {
                        Estado estado = new Estado { Id = (int)i.ESTADO.ID, Nombre = i.ESTADO.NOMBRE, Descripcion = i.ESTADO.DESCRIPCION };
                        Pedido pedido = new Pedido((int)i.ID, i.FECHA.ToString("dd/MM/yyyy"), estado, Direccion.GetDireccionById((int)i.DIRECCION_ID), Pedido.GetTotal((int)i.ID));
                        lista.Add(pedido);
                    }
                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
        }


        public static string PedidosPendientes(int idProductor)
        {
            var lista = new List<Pedido>();
            var bd = new ModeloFVEntities();
            try
            {

                var pedidos = bd.PRODUCTOR.Find(idProductor).PEDIDO.Where(a => a.ESTADO_ID == 2);

                foreach (var i in pedidos)
                {
                    Estado estado = new Estado { Id = (int)i.ESTADO.ID, Nombre = i.ESTADO.NOMBRE, Descripcion = i.ESTADO.DESCRIPCION };

                    Pedido p = new Pedido((int)i.ID, i.FECHA.ToString("dd/MM/yyyy"), estado, Direccion.GetDireccionById((int)i.DIRECCION_ID), Pedido.GetTotal((int)i.ID));
                    lista.Add(p);

                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }

        }


    }
}
