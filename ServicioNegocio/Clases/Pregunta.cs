﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using Newtonsoft.Json;
namespace ServicioNegocio.Clases
{
    public class Pregunta
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public static string GetPreguntas()
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Pregunta>();

            try
            {
                foreach (var pregunta in bd.PREGUNTA)
                {
                    lista.Add(new Pregunta() { Id = (int)pregunta.ID, Nombre = pregunta.NOMBRE });
                }
                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
        }
    }
}
