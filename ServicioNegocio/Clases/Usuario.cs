﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Win32;
using ServicioDatos;
using SimpleCrypto;


namespace ServicioNegocio.Clases
{

    public class Usuario
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int TipoUsuario { get; set; }
        public int Telefono { get; set; }
        public string ResetToken { get; set; }


        public bool ResetPassword()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var usuario = bd.USUARIO.Single(a => a.RESET_TOKEN == this.ResetToken);

                ICryptoService cryptoService = new PBKDF2();
                cryptoService.HashIterations = 25000;

                usuario.PASSWORD = cryptoService.Compute(this.Password);
                usuario.SALT = cryptoService.Salt;
                usuario.RESET_TOKEN = string.Empty;

                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool GetByEmail()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var usuario = bd.USUARIO.Single(a => a.EMAIL == this.Email);
                this.Id = (int)usuario.ID;
                do
                {
                    this.ResetToken = GeneradorToken.Generar();

                } while (Utils.ResetTokenExists(this.ResetToken));

                bd.USUARIO.Find(this.Id).RESET_TOKEN = this.ResetToken;
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool IsTokenValid(int idUsuario, string token)
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var user = bd.USUARIO.Single(a => a.ID == idUsuario && a.TOKEN == token);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static TokenMessage IniciarSesion(Usuario userjson)
        {
            TokenMessage TM = new TokenMessage();
            try
            {
                ModeloFVEntities bd = new ModeloFVEntities();
                var user = bd.USUARIO.FirstOrDefault(a => a.EMAIL == userjson.Email);

                if (user is null)
                {
                    TM.Message = "Datos invalidos";
                    return TM;
                }

                // Hashear password recibida
                ICryptoService o = new PBKDF2();
                o.HashIterations = 25000;
                if (o.Compute(userjson.Password, user.SALT).Equals(user.PASSWORD))
                {
                    // Si e-mail y contraseña coinciden
                    TM.Token = GeneradorToken.Generar();
                    TM.Message = "Ingreso correcto";
                    TM.TipoUsuario = (int)user.TIPO_USUARIO;
                    TM.Email = user.EMAIL;
                    TM.Id = (int)user.ID;
                    TM.Rut = user.RUT;
                    TM.Nombre = user.NOMBRE;
                    TM.Apellido = user.APELLIDO;
                    //Actualizar token de sesión en BD
                    user.TOKEN = TM.Token;
                    bd.SaveChanges();
                }
                else
                {
                    TM.Message = "Datos invalidos";
                }

                return TM;
            }
            catch (Exception)
            {
                // Devolver TM con mensaje de datos no validos
                TM.Message = "Datos invalidos";
                return TM;
            }

        }

        public static bool CambiarContrasena(int idUsuario, string oldPassword, string newPassword)
        {
            ICryptoService cryptoService = new PBKDF2();
            cryptoService.HashIterations = 25000;
            ModeloFVEntities bd = new ModeloFVEntities();

            try
            {
                var usuario = bd.USUARIO.Find(idUsuario);
                string oldHash = usuario.PASSWORD;
                string oldSalt = usuario.SALT;

                if (cryptoService.Compute(oldPassword, oldSalt).Equals(oldHash))
                {
                    ICryptoService cryptoService2 = new PBKDF2();
                    cryptoService2.HashIterations = 25000;

                    usuario.PASSWORD = cryptoService2.Compute(newPassword);
                    usuario.SALT = cryptoService2.Salt;
                    bd.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}
