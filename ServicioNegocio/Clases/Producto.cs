﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using Newtonsoft.Json;


namespace ServicioNegocio.Clases
{
    public class Producto
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public List<Temporada> Temporadas { get; set; }
        public Calidad Calidad { get; set; }
        public Categoria Categoria { get; set; }

        public Producto()
        {
            this.Id = 0;
            this.Nombre = string.Empty;
        }
        public Producto(int id_producto, string nombre)
        {
            this.Id = id_producto;
            this.Nombre = nombre;
        }


        public void GetProductoById()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            var pro = bd.PRODUCTO.Find(this.Id);

            this.Nombre = pro.NOMBRE;
            this.Categoria = new Categoria() { Id = (int)pro.CATEGORIA_ID, Nombre = pro.CATEGORIA.NOMBRE };
        }

        public static List<Producto> GetProductos()
        {
            ModeloFVEntities bd = new ModeloFVEntities();

            List<Producto> lista = new List<Producto>();

            foreach(var pro in bd.PRODUCTO)
            {
                var temporadas = new List<Temporada>();

                foreach (var temp in pro.TEMPORADA)
                {


                    var temporada = new Temporada()
                    {
                        Id = (int)temp.ID,
                        Nombre = temp.NOMBRE
                    };

                    temporadas.Add(temporada);
                }

                Producto producto = new Producto
                {
                    Id = (int)pro.ID,
                    Nombre = pro.NOMBRE,
                    Temporadas = temporadas,
                    Categoria = new Categoria() { Id = (int)pro.CATEGORIA_ID, Nombre = pro.CATEGORIA.NOMBRE }
                };
                lista.Add(producto);
            }

            return lista;

        }

        public static string GetProductosByCategoria(int idCategoria)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            List<Producto> lista = new List<Producto>();

            try
            {

                var productos = bd.PRODUCTO.Where(a => a.CATEGORIA_ID == idCategoria);

                foreach (var pro in productos)
                {

                    var temporadas = new List<Temporada>();

                    foreach (var temp in pro.TEMPORADA)
                    {


                        var temporada = new Temporada()
                        {
                            Id = (int)temp.ID,
                            Nombre = temp.NOMBRE
                        };

                        temporadas.Add(temporada);
                    }

                    Producto producto = new Producto
                    {
                        Id = (int)pro.ID,
                        Nombre = pro.NOMBRE,
                        Temporadas = temporadas,
                        Categoria = new Categoria() { Id = (int)pro.CATEGORIA_ID, Nombre = pro.CATEGORIA.NOMBRE }
                    };
                    lista.Add(producto);
                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }

        }

    }
}
