﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class Reporte
    {

        public string Producto { get; set; }
        public int Toneladas { get; set; }

        public static string Generar(DateTime fechaInicio, DateTime fechaTermino)
        {

            // var fechaTermino = DateTime.Today;
            // var fechaInicio = fechaTermino.AddMonths(-3);
            var bd = new ModeloFVEntities();
            var reporte = new Reporte();
            var pedidos = bd.PEDIDO.Where(a => a.FECHA >= fechaInicio && a.FECHA <= fechaTermino && a.ESTADO_ID == 11); // Pedidos dentro del rango de fecha con estado "Entregado"

            var detallesReporte = new List<Reporte>();
            foreach (var p in pedidos)
            {
                var pedi = Pedido.GetPedido((int)p.ID);
                var detalle = pedi.DetallePedido;
                foreach (var d in detalle)
                {
                    var rep = new Reporte();
                    rep.Producto = d.Producto.Nombre;
                    rep.Toneladas = d.Toneladas;

                    if(detallesReporte.Any(a => a.Producto == d.Producto.Nombre)) // Si es que ya existe, sumar las toneladas
                    {
                        detallesReporte[detallesReporte.FindIndex(a => a.Producto == d.Producto.Nombre)].Toneladas += d.Toneladas;
                    }
                    else // Si no existe, agregarlo a la lista
                    {
                        detallesReporte.Add(rep);
                    }

                }
                
            }

            detallesReporte = detallesReporte.OrderByDescending(a => a.Toneladas).Take(7).ToList(); // Ordenar por cantidad de toneladas de manera descendiente y tomar los primeros 7

            return JsonConvert.SerializeObject(detallesReporte);

        }


    }
}
