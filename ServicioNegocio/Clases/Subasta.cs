﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using Newtonsoft.Json;

namespace ServicioNegocio.Clases
{
    public class Subasta
    {
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public Pedido Pedido { get; set; }
        public Estado Estado { get; set; }
        public string FechaInicio { get; set; }
        public string FechaTermino { get; set; }
        public Bid YourBid { get; set; }
        public Bid LowestBid { get; set; }
        public bool BidEscogida { get; set; }
        public List<Transportista> Invitados { get; set; }
        public Transportista Ganador { get; set; }
        public bool RetiroCompleto { get; set; }

        public static bool Invitar(int idSubasta, List<Transportista> invitados)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var subasta = bd.SUBASTA.Find(idSubasta);

                foreach (var invitado in invitados)
                {
                    var transportista = bd.TRANSPORTISTA.Find(invitado.Id);
                    subasta.TRANSPORTISTA.Add(transportista);

                    // Notificar por correo
                    Mail mail = new Mail();
                    mail.Subject = $"Has sido invitado a participar en la subasta #{idSubasta}!";
                    mail.To = bd.USUARIO.Find(invitado.Id).EMAIL;
                    mail.Body = $"<span class='text-bold'>Estimado/a {transportista.USUARIO.NOMBRE} {transportista.USUARIO.APELLIDO}</span><br><br>Te informamos que has sido invitado a participar en la subasta #{idSubasta}.<br>Esta se encontrara disponible en tu sección de subastas activas, o puedes acceder directamente haciendo click <a href='#'>aquí</a>.";
                    mail.Send();
                }

                subasta.ESTADO_ID = 22;

                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string GetInvitados(int idSubasta)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Transportista>();

            try
            {
                var subasta = bd.SUBASTA.Find(idSubasta);
                foreach (var invitado in subasta.TRANSPORTISTA)
                {
                    lista.Add(new Transportista()
                    {
                        Id = (int)invitado.USUARIO_ID,
                        Nombre = invitado.USUARIO.NOMBRE,
                        Apellido = invitado.USUARIO.APELLIDO,
                        Email = invitado.USUARIO.EMAIL,
                        Telefono = (int)invitado.USUARIO.TELEFONO
                    });
                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
        }

        public static Subasta GetSubasta(int idSubasta)
        {
            Subasta subasta = new Subasta();
            ModeloFVEntities bd = new ModeloFVEntities();

            var sub = bd.SUBASTA.Find(idSubasta);

            subasta.Id = (int)sub.ID;
            subasta.IdPedido = (int)sub.PEDIDO.ID;
            subasta.Estado = new Estado() { Id = (int)sub.ESTADO.ID, Nombre = sub.ESTADO.NOMBRE, Descripcion = sub.ESTADO.DESCRIPCION };
            subasta.FechaInicio = sub.FECHA_INICIO.ToString("dd/MM/yyyy");
            subasta.FechaTermino = sub.FECHA_TERMINO.ToString("dd/MM/yyyy");

            var ped = bd.PEDIDO.Find(sub.PEDIDO_ID);
            if (ped.DE_PEDIDO.Count() == bd.DE_OFERTA.Where(a => a.PEDIDO_ID == sub.PEDIDO_ID && a.RETIRADO == "1").Count())
            {
                subasta.RetiroCompleto = true;
            }

            try
            {
                var ganador = bd.BID.Single(a => a.SUBASTA_ID == idSubasta && a.ESCOGIDA == "1");
                subasta.Ganador = new Transportista() { Id = (int)ganador.TRANSPORTISTA_ID };
            }
            catch (Exception)
            {

            }
            foreach (var i in sub.BID)
            {
                if (i.ESCOGIDA == null)
                {
                    subasta.BidEscogida = false;
                }
                else
                {
                    subasta.BidEscogida = true;
                    break;
                }
            }

            try
            {
                subasta.LowestBid = new Bid() { Precio = (int)sub.BID.Min(a => a.PRECIO) };
            }
            catch (Exception)
            {
                subasta.LowestBid = new Bid() { Precio = 0 };
            }

            return subasta;
        }

        public static bool ToggleRetirar(int pedidoId, int productoId, int productorId, int calidadId)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var oferta = bd.DE_OFERTA.Single(a => a.PEDIDO_ID == pedidoId && a.PRODUCTO_ID == productoId && a.PRODUCTOR.USUARIO.ID == productorId && a.CALIDAD_ID == calidadId);

                if (oferta.RETIRADO == "1")
                    oferta.RETIRADO = "0";
                else
                    oferta.RETIRADO = "1";

                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<Subasta> GetAllSubastas()
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            List<Subasta> lista = new List<Subasta>();

            foreach (var subasta in bd.SUBASTA)
            {
                var obj = new Subasta();
                obj.Id = (int)subasta.ID;
                obj.IdPedido = (int)subasta.PEDIDO_ID;
                obj.Estado = new Estado() { Id = (int)subasta.ESTADO.ID, Nombre = subasta.ESTADO.NOMBRE, Descripcion = subasta.ESTADO.DESCRIPCION };
                obj.FechaInicio = subasta.FECHA_INICIO.ToString("dd/MM/yyyy");
                obj.FechaTermino = subasta.FECHA_TERMINO.ToString("dd/MM/yyyy");

                lista.Add(obj);
            }

            return lista;

        }

        public static List<Subasta> GetSubastasActivas()
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            List<Subasta> lista = new List<Subasta>();
            var subastas = bd.SUBASTA.Where(a => a.ESTADO.ID == 21 || a.ESTADO.ID == 22 || a.ESTADO.ID == 23);

            foreach (var subasta in subastas)
            {
                var obj = new Subasta();
                obj.Id = (int)subasta.ID;
                obj.IdPedido = (int)subasta.PEDIDO_ID;
                obj.Estado = new Estado() { Id = (int)subasta.ESTADO.ID, Nombre = subasta.ESTADO.NOMBRE, Descripcion = subasta.ESTADO.DESCRIPCION };
                try
                {
                    obj.LowestBid = new Bid() { Precio = (int)subasta.BID.Min(a => a.PRECIO) };
                }
                catch (Exception)
                {
                    obj.LowestBid = new Bid() { Precio = 0 };
                }

                obj.FechaInicio = subasta.FECHA_INICIO.ToString("dd/MM/yyyy");
                obj.FechaTermino = subasta.FECHA_TERMINO.ToString("dd/MM/yyyy");

                lista.Add(obj);
            }

            return lista;
        }

        public static List<Subasta> GetSubastasFinalizadas() // Finalizadas y canceladas
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            List<Subasta> lista = new List<Subasta>();
            var subastas = bd.SUBASTA.Where(a => a.ESTADO.ID == 23 || a.ESTADO.ID == 24);

            foreach (var subasta in subastas)
            {
                var obj = new Subasta();
                obj.Id = (int)subasta.ID;
                obj.IdPedido = (int)subasta.PEDIDO_ID;
                obj.Estado = new Estado() { Id = (int)subasta.ESTADO.ID, Nombre = subasta.ESTADO.NOMBRE, Descripcion = subasta.ESTADO.DESCRIPCION };
                obj.LowestBid = new Bid() { Precio = (int)subasta.BID.Min(a => a.PRECIO) };
                obj.FechaInicio = subasta.FECHA_INICIO.ToString("dd/MM/yyyy");
                obj.FechaTermino = subasta.FECHA_TERMINO.ToString("dd/MM/yyyy");

                lista.Add(obj);
            }

            return lista;
        }

        public static bool FinalizarSubasta(int idSubasta)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var subasta = bd.SUBASTA.Find(idSubasta);

                var pedido = bd.PEDIDO.Find(subasta.PEDIDO_ID);

                subasta.ESTADO_ID = 24; // Finalizada
                pedido.ESTADO_ID = 6; // En recolección

                bd.SaveChanges();

                // Transportista ganador
                var ganador = bd.BID.Single(a => a.SUBASTA_ID == idSubasta && a.ESCOGIDA == "1");

                // Notificar a los transportistas que la subasta ha finalizado
                foreach (var transportista in subasta.TRANSPORTISTA)
                {
                    Mail mail = new Mail();
                    mail.Subject = $"La subasta #{idSubasta} ha finalizado.";
                    mail.To = transportista.USUARIO.EMAIL;

                    // Si el transportista es el ganador de la subasta
                    if (transportista.USUARIO_ID == ganador.TRANSPORTISTA_ID)
                    {
                        mail.Body = $"<span class='text-bold'>Estimado/a {transportista.USUARIO.NOMBRE} {transportista.USUARIO.APELLIDO}</span><br><br>Te informamos que has sido escogido ganador de la subasta #{idSubasta}.<br>A continuación se incluye un detalle de los productores de los cuales debes retirar los productos:<br><br>" +
                            $@"Para mas detalles puedes visitar la subasta en nuestro sitio web haciendo click <a href='#'>aquí</a>.";
                    }
                    else
                    {
                        mail.Body = $"<span class='text-bold'>Estimado/a {transportista.USUARIO.NOMBRE} {transportista.USUARIO.APELLIDO}</span><br><br>Te informamos que la subasta #{idSubasta} ha finalizado.<br>Lamentablemente no has ganado esta vez, pero te invitamos a participar en nuestras proximas subastas.<br>Agradecemos tu participación.";
                    }

                    mail.Send();
                }

                // Notificar a los productores
                var OfertasEscogidas = bd.DE_ESCOGIDO.Where(a => a.PEDIDO_ID == pedido.ID);
                var productores = new List<int>();
                foreach (var oferta in OfertasEscogidas)
                {
                    var productor = bd.PRODUCTOR.Find(oferta.PRODUCTOR_ID).USUARIO_ID;
                    // Solo agregar correo si es que no se encuentra en la lista, para evitar duplicados
                    if (!productores.Contains((int)productor))
                        productores.Add((int)productor);
                }

                foreach (var i in productores)
                {
                    var mail = new Mail();
                    var productor = bd.USUARIO.Find(i);
                    mail.Subject = $"El transportista para el pedido #{ganador.SUBASTA.PEDIDO_ID} ha sido escogido.";
                    mail.To = bd.USUARIO.Find(i).EMAIL;
                    var transportistaGanador = bd.TRANSPORTISTA.Find(ganador.TRANSPORTISTA_ID).USUARIO;
                    mail.Body = $"<span class='text-bold'>Estimado/a {productor.NOMBRE} {productor.APELLIDO}</span><br><br>Te informamos que el transportista para el pedido #{pedido.ID} ha sido escogido.<br>Este debera hacer retiro de los productos que ofertaste<br> a continuación se incluyen los datos de la empresa transportista:<br><br>" +
                        $"{transportistaGanador.NOMBRE} {transportistaGanador.APELLIDO}<br>" +
                        $"{transportistaGanador.EMAIL}<br>" +
                        $"+569 {transportistaGanador.TELEFONO}";

                    mail.Send();
                }

                // Notificar al cliente de que la subasta ha concluido y el transportista ha sido escogido
                var email = new Mail();
                var cliente = pedido.CLIENTE.USUARIO;
                var transpGanador = bd.TRANSPORTISTA.Find(ganador.TRANSPORTISTA_ID).USUARIO;

                email.Subject = $"El transportista para tu pedido #{ganador.SUBASTA.PEDIDO_ID} ha sido escogido.";
 
                email.Body = $"<span class='text-bold'>Estimado/a {cliente.NOMBRE} {cliente.APELLIDO}</span><br><br>Te informamos que la subasta #{idSubasta} para tu pedido #{pedido.ID} ha finalizado y el transportista ha sido escogido.<br>A continuación se incluyen los datos de la empresa transportista:<br><br>" +
                    $"{transpGanador.NOMBRE} {transpGanador.APELLIDO}<br>" +
                    $"{transpGanador.EMAIL}<br>" +
                    $"+569 {transpGanador.TELEFONO}";
                email.Send();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
