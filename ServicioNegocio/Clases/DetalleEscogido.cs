﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class DetalleEscogido
    {

        public int IdPedido { get; set; }
        public int IdProducto { get; set; }
        public int IdCalidad { get; set; }
        public string RutProductor { get; set; }
        public int Toneladas { get; set; }
        public Productor Productor { get; set; }

        public static int GetTotalDetalle(int id_pedido, int id_producto)
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            int total = 0;

            //try
            //{
            //    var pedido = bd.PEDIDO.Single(a => a.ID == id_pedido);

            //    var ofertas = bd.DETALLE_OFERTA.Where(a => a.PEDIDO.ID == id_pedido && a.PRODUCTO.ID == id_producto);
            //    var escogidas = bd.DETALLE_ESCOGIDO.Where(a => a.PEDIDO.ID == id_pedido && a.PRODUCTO.ID == id_producto);
            //    var ek = bd.DETALLE_ESCOGIDO.Single(a => a.PEDIDO.ID == id_pedido);
            //    string jsonEnviar = JsonConvert.SerializeObject(escogidas);


            //    foreach (var a in escogidas)
            //    {
            //        var kilos = (int)a.KILOS_ESCOGIDOS;
            //        total += (int)a.KILOS_ESCOGIDOS * (int)bd.DETALLE_OFERTA.Single(b => b.PEDIDO.ID == a.PEDIDO_ID && b.PRODUCTO.ID == a.PRODUCTO.ID && b.CALIDAD.ID == a.CALIDAD.ID).PRECIO_KILO;
            //    }

            //}
            //catch (Exception)
            //{
            //    return 0;
            //}

            int precioKilo = 0;
            int kilosEscogidos = 0;

            try
            {
                var ofertaEscogida = bd.DE_ESCOGIDO.Single(a => a.PEDIDO_ID == id_pedido && a.PRODUCTO_ID == id_producto);
                kilosEscogidos = (int)bd.DE_ESCOGIDO.Single(a => a.PEDIDO_ID == id_pedido && a.PRODUCTO_ID == id_producto).TONELADAS;
                precioKilo = (int)bd.DE_OFERTA.Single(a => a.PEDIDO_ID == id_pedido && a.PRODUCTO_ID == id_producto && a.PRODUCTOR_ID == ofertaEscogida.PRODUCTOR_ID).PRECIO;
            }
            catch (Exception)
            {
                
            }

            total = kilosEscogidos * precioKilo;

            return total;

        }

        public static List<DetalleEscogido> GetOfertasEscogidas(int idPedido, int idProducto, int idCalidad)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<DetalleEscogido>();

            try
            {
                var ofertas = bd.DE_ESCOGIDO.Where(a => a.PEDIDO_ID == idPedido && a.PRODUCTO_ID == idProducto && a.CALIDAD_ID == idCalidad);
                
                foreach (var i in ofertas)
                {
                    var oferta = new DetalleEscogido()
                    { IdPedido = (int)i.PEDIDO_ID,
                        IdProducto = (int)i.PRODUCTO_ID,
                        IdCalidad = (int)i.CALIDAD_ID,
                        Productor = new Productor() { Id = (int)i.PRODUCTOR_ID},
                        Toneladas = (int)i.TONELADAS
                    };

                    lista.Add(oferta);

                }

                return lista;
            }
            catch (Exception)
            {
                return lista;
            }


        }


    }
}
