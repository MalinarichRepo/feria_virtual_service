﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioNegocio.Clases
{
    public class Respuesta
    {
        public Pregunta Pregunta { get; set; }
        public int Puntaje { get; set; }
    }
}
