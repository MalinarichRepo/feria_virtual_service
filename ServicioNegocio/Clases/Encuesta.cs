﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using Newtonsoft.Json;

namespace ServicioNegocio.Clases
{
    public class Encuesta
    {

        public int Id { get; set; }
        public List<Respuesta> Respuestas { get; set; }


        public static bool Enviar(string encuestaJson)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var encuesta = JsonConvert.DeserializeObject<Encuesta>(encuestaJson);

                var enc = new ENCUESTA() { PEDIDO_ID = encuesta.Id };
                bd.ENCUESTA.Add(enc);
                foreach (var respuesta in encuesta.Respuestas)
                {
                    var res = new RESPUESTA() { ENCUESTA_ID = encuesta.Id, PREGUNTA_ID = respuesta.Pregunta.Id, PUNTAJE = respuesta.Puntaje };
                    //res.ENCUESTA = bd.ENCUESTA.Find(encuesta.Id);
                    //res.PREGUNTA = bd.PREGUNTA.Find(respuesta.Pregunta.Id);
                    bd.RESPUESTA.Add(res);
                }
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
