﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using SimpleCrypto;
using Newtonsoft.Json;
namespace ServicioNegocio.Clases
{
    public class Transportista : Usuario
    {

        public Usuario usuario { get; set; }
        public int EmpresaID { get; set; }

        public static string All()
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Transportista>();

            try
            {
                foreach (var transportista in bd.TRANSPORTISTA)
                {
                    var nuevo = new Transportista()
                    {Id = (int)transportista.USUARIO_ID,
                    Nombre = transportista.USUARIO.NOMBRE,
                    Apellido = transportista.USUARIO.APELLIDO,
                    Email = transportista.USUARIO.EMAIL,
                    Rut = transportista.USUARIO.RUT,
                    Telefono = (int)transportista.USUARIO.TELEFONO,
                    TipoUsuario = (int)transportista.USUARIO.TIPO_USUARIO};

                    lista.Add(nuevo);

                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
        }

        public bool Registrar()
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            TRANSPORTISTA tra = new TRANSPORTISTA();
            USUARIO us = new USUARIO();

            us.ID = Utils.GetNextUserId();
            us.RUT = this.Rut;
            us.NOMBRE = this.Nombre;
            us.APELLIDO = this.Apellido;
            us.EMAIL = this.Email;
            ICryptoService cryptoService = new PBKDF2();
            cryptoService.HashIterations = 25000;

            Mail email = new Mail();
            email.Subject = "Bienvenido a FeriaVirtual! Tu cuenta ya se encuentra disponible.";
            email.To = us.EMAIL;

            string contra = email.EnviarContrasenia(this);

            string Password = cryptoService.Compute(contra);
            string Salt = cryptoService.Salt;

            us.PASSWORD = Password;
            us.SALT = Salt;
            us.TIPO_USUARIO = 3;
            us.TOKEN = "0";
            us.TELEFONO = this.Telefono;
            us.RESET_TOKEN = "";
            tra.USUARIO = us;
            //tra.EMPRESA_ID = this.EmpresaID;

            bd.TRANSPORTISTA.Add(tra);
            bd.SaveChanges();

            return true;

        }

        public static string GetSubastasActivas(int idTransportista)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Subasta>();

            try
            {
                var subastas = bd.TRANSPORTISTA.Find(idTransportista).SUBASTA.Where(a => a.ESTADO.ID == 21 || a.ESTADO.ID == 22);

                foreach (var subasta in subastas)
                {
                    var obj = new Subasta();
                    obj.Id = (int)subasta.ID;
                    obj.IdPedido = (int)subasta.PEDIDO_ID;
                    obj.Estado = new Estado() { Id = (int)subasta.ESTADO.ID, Nombre = subasta.ESTADO.NOMBRE, Descripcion = subasta.ESTADO.DESCRIPCION };
                    try
                    {
                        obj.LowestBid = new Bid() { Precio = (int)subasta.BID.Min(a => a.PRECIO) };
                    }
                    catch (Exception)
                    {
                        obj.LowestBid = new Bid() { Precio = 0 };
                    }
                    obj.YourBid = Bid.GetYourBid((int)subasta.ID, idTransportista);
                    obj.FechaInicio = subasta.FECHA_INICIO.ToString("dd/MM/yyyy");
                    obj.FechaTermino = subasta.FECHA_TERMINO.ToString("dd/MM/yyyy");

                    lista.Add(obj);
                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
        }

        public static string GetSubastasFinalizadas(int idTransportista)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<Subasta>();

            try
            {
                var subastas = bd.TRANSPORTISTA.Find(idTransportista).SUBASTA.Where(a => a.ESTADO.ID == 23 || a.ESTADO.ID == 24);

                foreach (var subasta in subastas)
                {
                    var obj = new Subasta();
                    obj.Id = (int)subasta.ID;
                    obj.IdPedido = (int)subasta.PEDIDO_ID;
                    obj.Estado = new Estado() { Id = (int)subasta.ESTADO.ID, Nombre = subasta.ESTADO.NOMBRE, Descripcion = subasta.ESTADO.DESCRIPCION };
                    try
                    {
                        obj.LowestBid = new Bid() { Precio = (int)subasta.BID.Min(a => a.PRECIO) };
                    }
                    catch (Exception)
                    {
                        obj.LowestBid = new Bid() { Precio = 0 };
                    }
                    obj.YourBid = Bid.GetYourBid((int)subasta.ID, idTransportista);
                    obj.FechaInicio = subasta.FECHA_INICIO.ToString("dd/MM/yyyy");
                    obj.FechaTermino = subasta.FECHA_TERMINO.ToString("dd/MM/yyyy");

                    lista.Add(obj);
                }

                return JsonConvert.SerializeObject(lista);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(lista);
            }
        }


    }
}
