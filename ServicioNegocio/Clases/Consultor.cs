﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using SimpleCrypto;

namespace ServicioNegocio.Clases
{
    public class Consultor : Usuario
    {
        public Usuario usuario { get; set; }

        public bool Registrar()
        {
            ModeloFVEntities bd = new ModeloFVEntities();
            CONSULTOR con = new CONSULTOR();
            USUARIO us = new USUARIO();

            us.ID = Utils.GetNextUserId();
            us.RUT = this.Rut;
            us.NOMBRE = this.Nombre;
            us.APELLIDO = this.Apellido;
            us.EMAIL = this.Email;
            ICryptoService cryptoService = new PBKDF2();
            cryptoService.HashIterations = 25000;
            string Password = cryptoService.Compute(this.Password);
            string Salt = cryptoService.Salt;

            us.PASSWORD = Password;
            us.SALT = Salt;
            us.TIPO_USUARIO = 5;
            us.TOKEN = "0";
            us.TELEFONO = this.Telefono;
            us.RESET_TOKEN = "";
            con.USUARIO = us;

            bd.CONSULTOR.Add(con);
            bd.SaveChanges();

            return true;

        }

    }
}
