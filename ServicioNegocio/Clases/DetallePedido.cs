﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;

namespace ServicioNegocio.Clases
{
    public class DetallePedido
    {
        public Producto Producto { get; set; }
        public Calidad Calidad { get; set; }
        public int Toneladas { get; set; }
        public List<DetalleOferta> Ofertas { get; set; }
        public bool SeHaEscogidoOferta { get; set; }

        public static List<DetallePedido> GetDetallePedido(int idPedido)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<DetallePedido>();

            var detalles = bd.DE_PEDIDO.Where(a => a.PEDIDO.ID == idPedido);

            foreach (var detalle in detalles)
            {
                var dp = new DetallePedido()
                {
                    Producto = new Producto() { Id = (int)detalle.PRODUCTO.ID, Nombre = detalle.PRODUCTO.NOMBRE },
                    Calidad = new Calidad() { Id = (int)detalle.CALIDAD.ID, Nombre = detalle.CALIDAD.NOMBRE },
                    Toneladas = (int)detalle.TONELADAS,
                                        
                };

                var ofertas = new List<DetalleOferta>();
                foreach (var i in detalle.DE_OFERTA)
                {

                    var d = new DetalleOferta()
                    {
                        Precio = (int)i.PRECIO,
                        Productor = new Productor() {Id = (int)i.PRODUCTOR.USUARIO_ID, Nombre = i.PRODUCTOR.USUARIO.NOMBRE, Apellido = i.PRODUCTOR.USUARIO.APELLIDO },
                        IdProductor = (int)i.PRODUCTOR.USUARIO_ID,
                        Toneladas = (int)i.TONELADAS,
                        TotalOferta = (int)i.TONELADAS * (int)i.PRECIO
                    };

                    if (i.RETIRADO == "1")
                        d.Retirado = true;

                    try
                    {
                        bd.DE_ESCOGIDO.Single(a => a.PEDIDO_ID == i.PEDIDO_ID && a.PRODUCTO_ID == i.PRODUCTO_ID && a.CALIDAD_ID == i.CALIDAD_ID && a.PRODUCTOR_ID == i.PRODUCTOR_ID);

                        d.Escogida = true;
                        dp.SeHaEscogidoOferta = true;

                    }
                    catch (Exception)
                    {
                        d.Escogida = false;

                        // Solo cambiar si es que la variable no es true (para evitar que se sobreescriba una vez ha sido marcada true)
                        if (!dp.SeHaEscogidoOferta)
                        {
                            dp.SeHaEscogidoOferta = false;
                        }
                    }

                    d.OfertasEscogidas = DetalleEscogido.GetOfertasEscogidas((int)i.PEDIDO_ID, (int)i.PRODUCTO_ID, (int)i.CALIDAD_ID);

                    ofertas.Add(d);
                }

                dp.Ofertas = ofertas;

                lista.Add(dp);
            }

            return lista;

        }

        public static List<DetallePedido> GetDetallePedido(int idPedido, int idProductor)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<DetallePedido>();

            var detalles = bd.DE_PEDIDO
                .Join(bd.PROD_OFRECIDOS,
                a => a.PRODUCTO_ID,
                b => b.PRODUCTO_ID,
                (a, b) => new { Detalle = a, ProductosOfrecidos = b })
                .Where(a => a.ProductosOfrecidos.PRODUCTOR_ID == idProductor && a.Detalle.PEDIDO_ID == idPedido)
                ;
                


            foreach (var detalle in detalles)
            {
                var dp = new DetallePedido()
                {
                    Producto = new Producto() { Id = (int)detalle.Detalle.PRODUCTO.ID, Nombre = detalle.Detalle.PRODUCTO.NOMBRE },
                    Calidad = new Calidad() { Id = (int)detalle.Detalle.CALIDAD.ID, Nombre = detalle.Detalle.CALIDAD.NOMBRE },
                    Toneladas = (int)detalle.Detalle.TONELADAS
                };

                var ofertas = new List<DetalleOferta>();
                foreach (var i in detalle.Detalle.DE_OFERTA.Where(a => a.PRODUCTOR_ID == idProductor))
                {
                    var d = new DetalleOferta()
                    {
                        Precio = (int)i.PRECIO,
                        IdProductor = (int)i.PRODUCTOR.USUARIO_ID
                    };

                    ofertas.Add(d);
                }

                dp.Ofertas = ofertas;

                lista.Add(dp);
            }

            return lista;

        }


        public static DetallePedido GetDetallePedidoById(int idPedido, int idProducto, int idCalidad)
        {
            var bd = new ModeloFVEntities();
            var detalle = new DetallePedido();

            try
            {
                var c = bd.DE_PEDIDO.Single(a => a.PEDIDO.ID == idPedido && a.PRODUCTO.ID == idProducto && a.CALIDAD.ID == idCalidad);

                //detalle.Pedido = new Pedido()
                //{ Id = idPedido };
                detalle.Producto = new Producto() { Id = (int)c.PRODUCTO.ID, Nombre = c.PRODUCTO.NOMBRE };
                detalle.Calidad = new Calidad() {Id = (int)c.CALIDAD.ID, Nombre = c.CALIDAD.NOMBRE };
                detalle.Toneladas = (int)c.TONELADAS;
                var ofertas = new List<DetalleOferta>();
                foreach (var i in c.DE_OFERTA)
                {
                    var d = new DetalleOferta()
                    {
                        Precio = (int)i.PRECIO
                    };

                    d.TotalOferta = d.Precio * d.Toneladas;
                    d.IdPedido = (int)i.PEDIDO_ID;
                    d.IdProducto = (int)i.PRODUCTO_ID;
                    d.IdCalidad = (int)i.CALIDAD_ID;
                    d.Precio = (int)i.PRECIO;
                    d.Toneladas = (int)i.TONELADAS;
                    d.TotalOferta = (int)(i.PRECIO * i.TONELADAS);
                    d.Productor = new Productor { Id = (int)i.PRODUCTOR_ID, Nombre = i.PRODUCTOR.USUARIO.NOMBRE, Apellido = i.PRODUCTOR.USUARIO.APELLIDO };

                    try
                    {
                        bd.DE_ESCOGIDO.Single(a => a.PEDIDO_ID == i.PEDIDO_ID && a.PRODUCTO_ID == i.PRODUCTO_ID && a.CALIDAD_ID == i.CALIDAD_ID && a.PRODUCTOR_ID == i.PRODUCTOR_ID);

                        d.Escogida = true;
                        detalle.SeHaEscogidoOferta = true;

                    }
                    catch (Exception)
                    {
                        d.Escogida = false;

                        // Solo cambiar si es que la variable no es true (para evitar que se sobreescriba una vez ha sido marcada true)
                        if (!detalle.SeHaEscogidoOferta)
                        {
                            detalle.SeHaEscogidoOferta = false;
                        }
                    }

                    ofertas.Add(d);
                }

                detalle.Ofertas = ofertas;

                return detalle;
            }
            catch (Exception)
            {
                return detalle;
            }

        }

    }
}
