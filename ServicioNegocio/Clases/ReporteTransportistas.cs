﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
using Newtonsoft.Json;

namespace ServicioNegocio.Clases
{
    public class ReporteTransportistas
    {

        public Transportista Transportista { get; set; }
        public double Puntuacion { get; set; }

        public static string Generar(int rango)
        {
            var bd = new ModeloFVEntities();
            var lista = new List<ReporteTransportistas>();

            // Agregar todos los transportistas a la lista, con una puntuacion inicial de 0.
            foreach (var transportista in bd.TRANSPORTISTA)
            {
                lista.Add(new ReporteTransportistas() { Transportista = new Transportista() { Id = (int)transportista.USUARIO_ID, Nombre = $"{transportista.USUARIO.NOMBRE} {transportista.USUARIO.APELLIDO}" }, Puntuacion = 0 });
            }

            // Lista con pedidos en los que se ha respondido la encuesta de satisfacción.
            var pedidos = bd.PEDIDO.Where(a => a.ENCUESTA.RESPUESTA.Count() == 5);

            foreach (var pedido in pedidos)
            {
                var subasta = bd.SUBASTA.Single(a => a.PEDIDO_ID == pedido.ID);
                var ofertaGanadora = bd.BID.Single(a => a.SUBASTA_ID == subasta.ID && a.ESCOGIDA == "1");
                var transportistaGanador = bd.TRANSPORTISTA.Find(ofertaGanadora.TRANSPORTISTA_ID).USUARIO;

                int puntaje = (int)pedido.ENCUESTA.RESPUESTA.Single(a => a.PREGUNTA_ID == 4).PUNTAJE;
                double puntajeActual = lista.Single(a => a.Transportista.Id == transportistaGanador.ID).Puntuacion;

                if(puntajeActual == 0)
                    lista.Single(a => a.Transportista.Id == transportistaGanador.ID).Puntuacion = puntaje;
                else
                    lista.Single(a => a.Transportista.Id == transportistaGanador.ID).Puntuacion = (puntajeActual + puntaje) / 2;


            }

            lista = lista.OrderByDescending(a => a.Puntuacion).ToList();

            return JsonConvert.SerializeObject(lista.Take(rango).ToList());

        }

    }
}
