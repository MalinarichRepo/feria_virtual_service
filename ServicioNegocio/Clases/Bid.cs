﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioDatos;
namespace ServicioNegocio.Clases
{
    public class Bid
    {
        public Subasta Subasta { get; set; }
        public Transportista Transportista { get; set; }
        public int Precio { get; set; }
        public string Fecha { get; set; }
        public bool Escogida { get; set; }

        public static Bid GetYourBid(int idSubasta, int idTransportista)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var yourBid = bd.BID.Find(idSubasta, idTransportista);

                if (yourBid is null)
                    return new Bid() { Precio = 0 };
                    
                else
                    return new Bid() { Precio = (int)yourBid.PRECIO };
            }
            catch (Exception)
            {
                return new Bid() { Precio = 0 };
            }
        }
        public bool Ofertar()
        {
            var bd = new ModeloFVEntities();

            try
            {       
                var oferta = bd.BID.Find(this.Subasta.Id, this.Transportista.Id);
                var subasta = bd.SUBASTA.Find(this.Subasta.Id);

                // Si la subasta no se encuentra activa, retornar false
                if (subasta.ESTADO.ID != 22)
                {
                    return false;
                }
                
                // Comprobar que la oferta nueva es menor que la anterior
                // Si existen ofertas previas
                //if (subasta.BID.Count() > 0)
                //{
                    // Si la oferta nueva es mayor o igual a la oferta previa retornar false
                    // Para no poder sobreescribir la oferta mas baja
                    //if (this.Precio >= subasta.BID.Min(a => a.PRECIO))
                    //{
                    //    return false;
                    //}
                //}

                // La oferta no puede tener costo 0 o menor
                if (this.Precio < 1)
                {
                    return false;
                }

                // Si oferta no existe, crearla
                if (oferta is null) 
                {
                    var nueva_oferta = new BID
                    {
                        SUBASTA_ID = this.Subasta.Id,
                        TRANSPORTISTA_ID = this.Transportista.Id,
                        PRECIO = this.Precio,
                        FECHA = DateTime.Now
                    };
                    bd.BID.Add(nueva_oferta);
                }
                // Si oferta ya existe, actualizarla
                else
                {
                    oferta.PRECIO = this.Precio;
                    oferta.FECHA = DateTime.Now;
                }

                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<Bid> GetBids(int idSubasta)
        {
            var lista = new List<Bid>();
            var bd = new ModeloFVEntities();

            try
            {
                var bids = bd.BID.Where(a => a.SUBASTA_ID == idSubasta);

                foreach (var i in bids)
                {
                    var bid = new Bid
                    {
                        Transportista = new Transportista() { Id = (int)i.TRANSPORTISTA_ID, Nombre = i.TRANSPORTISTA.USUARIO.NOMBRE, Apellido = i.TRANSPORTISTA.USUARIO.APELLIDO },
                        Subasta = new Subasta() { Id = idSubasta },
                        Fecha = i.FECHA.ToString("dd/MM/yyyy"),
                        Precio = (int)i.PRECIO
                    };
                    if (i.ESCOGIDA == null)
                        bid.Escogida = false;
                    else
                        bid.Escogida = true;
                    lista.Add(bid);

                }

                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
        public static bool EscogerBid(int idSubasta, int idTransportista)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var bid = bd.BID.Single(a => a.SUBASTA_ID == idSubasta && a.TRANSPORTISTA_ID == idTransportista);
                bid.ESCOGIDA = "1";

                try
                {
                    // Marcar otra bid como no escogida (si es que habia alguna bid previa)
                    bd.BID.Single(a => a.SUBASTA_ID == idSubasta && a.ESCOGIDA == "1").ESCOGIDA = "";
                }
                catch (Exception)
                {

                }
                
                bd.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}
