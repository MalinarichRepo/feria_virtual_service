﻿using ServicioDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioNegocio.Clases
{
    public class Direccion
    {
        public int Id { get; set; }
        public Productor Productor { get; set; }
        public string Nombre { get; set; }
        public string Pais { get; set; }
        public string Region { get; set; }
        public int CodigoPostal { get; set; }
        public string Detalle { get; set; }

        public static Direccion GetDireccionById(int id)
        {
            var bd = new ModeloFVEntities();
            Direccion direccion = new Direccion();
            try
            {
                var a = bd.DIRECCION.Find(id);
                direccion.Id = id;
                direccion.Nombre = a.NOMBRE;
                direccion.Pais = a.PAIS;
                direccion.Region = a.REGION;
                direccion.CodigoPostal = (int)a.CODIGO_POSTAL;
                direccion.Detalle = a.DETALLE;
                return direccion;
            }
            catch (Exception)
            {
                return direccion;
            }
        }
        public static List<Direccion> GetDireccionesCliente(int clienteId)
        {
            List<Direccion> lista = new List<Direccion>();
            var bd = new ModeloFVEntities();

            try
            {
                var direcciones = bd.DIRECCION.Where(a => a.CLIENTE_ID == clienteId);

                foreach (var d in direcciones)
                {
                    var direccion = new Direccion()
                    {
                        Id = (int)d.ID,
                        Nombre = d.NOMBRE,
                        Pais = d.PAIS,
                        Region = d.REGION,
                        CodigoPostal = (int)d.CODIGO_POSTAL,
                        Detalle = d.DETALLE
                    };

                    lista.Add(direccion);

                }

                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public static bool RemoveDireccion(int idDireccion)
        {
            var bd = new ModeloFVEntities();

            try
            {
                var direccion = bd.DIRECCION.Find(idDireccion);
                bd.DIRECCION.Remove(direccion);
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddDireccion()
        {
            var bd = new ModeloFVEntities();

            try
            {
                var nuevaDireccion = new DIRECCION();
                nuevaDireccion.ID = Utils.GetNextDireccionId();
                nuevaDireccion.CLIENTE_ID = this.Productor.Id;
                nuevaDireccion.NOMBRE = this.Nombre;
                nuevaDireccion.PAIS = this.Pais;
                nuevaDireccion.REGION = this.Region;
                nuevaDireccion.CODIGO_POSTAL = this.CodigoPostal;
                nuevaDireccion.DETALLE = this.Detalle;

                bd.DIRECCION.Add(nuevaDireccion);
                bd.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

    }
}
