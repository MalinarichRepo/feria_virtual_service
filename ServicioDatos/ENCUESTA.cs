//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicioDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class ENCUESTA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ENCUESTA()
        {
            this.RESPUESTA = new HashSet<RESPUESTA>();
        }
    
        public decimal PEDIDO_ID { get; set; }
    
        public virtual PEDIDO PEDIDO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RESPUESTA> RESPUESTA { get; set; }
    }
}
