//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicioDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class SUBASTA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SUBASTA()
        {
            this.BID = new HashSet<BID>();
            this.TRANSPORTISTA = new HashSet<TRANSPORTISTA>();
        }
    
        public decimal ID { get; set; }
        public decimal PEDIDO_ID { get; set; }
        public decimal ESTADO_ID { get; set; }
        public System.DateTime FECHA_INICIO { get; set; }
        public System.DateTime FECHA_TERMINO { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BID> BID { get; set; }
        public virtual ESTADO ESTADO { get; set; }
        public virtual PEDIDO PEDIDO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TRANSPORTISTA> TRANSPORTISTA { get; set; }
    }
}
