//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicioDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class DE_PAGO
    {
        public decimal PEDIDO_ID { get; set; }
        public decimal METODO_PAGO_ID { get; set; }
        public decimal TOTAL { get; set; }
        public string ESTADO { get; set; }
    
        public virtual METODO_PAGO METODO_PAGO { get; set; }
        public virtual PEDIDO PEDIDO { get; set; }
    }
}
