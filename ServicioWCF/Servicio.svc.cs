﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using ServicioNegocio.Clases;
using Newtonsoft.Json;
using ServicioDatos;

namespace ServicioWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Servicio" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Servicio.svc o Servicio.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Servicio : IServicio
    {
        public string ReporteTransportistas(int rango)
        {
            return ServicioNegocio.Clases.ReporteTransportistas.Generar(rango);
        }
        public bool HabilitarPago(int idPedido)
        {
            return Pedido.HabilitarPago(idPedido);
        }
        public bool EnviarEncuesta(string encuestaJson)
        {
            return Encuesta.Enviar(encuestaJson);
        }

        public string GetPreguntas()
        {
            return Pregunta.GetPreguntas();
        }

        public bool RealizarPago(int idPedido)
        {
            return Pedido.RealizarPago(idPedido);
        }

        public bool ConfirmarRecepcion(int idPedido)
        {
            return Pedido.ConfirmarRecepcion(idPedido);
        }
        public bool ConfirmarEntrega(int idPedido)
        {
            return Pedido.ConfirmarEntrega(idPedido);
        }
        public bool ConfirmarRetiro(int idPedido)
        {
            return Pedido.ConfirmarRetiro(idPedido);
        }

        public bool ToggleRetirar(int pedidoId, int productoId, int productorId, int calidadId)
        {
            return Subasta.ToggleRetirar(pedidoId, productoId, productorId, calidadId);
        }

        public string GetPosiblesProductores(int idPedido)
        {
            return Pedido.GetPosiblesProductores(idPedido);
        }

        public string GetAllTransportistas()
        {
            return Transportista.All();
        }
        public bool InvitarSubasta(int idSubasta, string invitados)
        {
            return Subasta.Invitar(idSubasta, JsonConvert.DeserializeObject<List<Transportista>>(invitados));
        }

        public bool InvitarPedido(int idPedido, string invitados)
        {
            return Pedido.Invitar(idPedido, JsonConvert.DeserializeObject<List<Productor>>(invitados));
        }

        public string GetInvitadosPedido(int idPedido)
        {
            return Pedido.GetInvitados(idPedido);
        }

        public string GetInvitadosSubasta(int idSubasta)
        {
            return Subasta.GetInvitados(idSubasta);
        }
        public string GetReporte(DateTime fechaInicio, DateTime fechaTermino)
        {
            return Reporte.Generar(fechaInicio, fechaTermino);
        }

        public string GetProductosByCategoria(int idCategoria)
        {
            return Producto.GetProductosByCategoria(idCategoria);
        }
        public bool EscogerBid(int idSubasta, int idTransportista)
        {
            return ServicioNegocio.Clases.Bid.EscogerBid(idSubasta, idTransportista);
        }
        public string GetBids(int idSubasta)
        {
            var bids = ServicioNegocio.Clases.Bid.GetBids(idSubasta);

            return JsonConvert.SerializeObject(bids);

        }

        public bool CancelarPedido(int idPedido)
        {
            return Pedido.CancelarPedido(idPedido);
        }

        public bool QuitarOferta(int idPedido, int idProducto, int idCalidad, int idProductor)
        {
            return DetalleOferta.QuitarOferta(idPedido, idProducto, idCalidad, idProductor);
        }
        public bool FinalizarSubasta(int idSubasta)
        {
            return Subasta.FinalizarSubasta(idSubasta);
        }
        public string GetYourBid(int idSubasta, int idTransportista)
        {
            var yourBid = ServicioNegocio.Clases.Bid.GetYourBid(idSubasta, idTransportista);

            return JsonConvert.SerializeObject(yourBid);
        }
        public bool Bid(string bid)
        {
            Bid oferta = JsonConvert.DeserializeObject<Bid>(bid);

            return oferta.Ofertar();

        }

        public string GetSubastasActivasWBid(int idTransportista)
        {
            //var subastas = Subasta.GetSubastasActivas(idTransportista);
            // return JsonConvert.SerializeObject(subastas);
            return Transportista.GetSubastasActivas(idTransportista);
        }
        public string GetSubastasActivas()
        {
            var subastas = Subasta.GetSubastasActivas();
            return JsonConvert.SerializeObject(subastas);
        }

        public string GetSubastasFinalizadasWBid(int idTransportista)
        {
            //var subastas = Subasta.GetSubastasFinalizadas(idTransportista);
            //return JsonConvert.SerializeObject(subastas);
            return Transportista.GetSubastasFinalizadas(idTransportista);
        }
        public string GetSubastasFinalizadas()
        {
            var subastas = Subasta.GetSubastasFinalizadas();
            return JsonConvert.SerializeObject(subastas);
        }

        public bool ConfirmarPedido(int idPedido)
        {
            return Pedido.ConfirmarPedido(idPedido);
        }
        public bool CompletarPedido(int idPedido)
        {
            return Pedido.CompletarPedido(idPedido);
        }

        public bool EscogerOferta(int idPedido, int idProducto, int idCalidad, int idProductor, int toneladas)
        {
            return DetalleOferta.EscogerOferta(idPedido, idProducto, idCalidad, idProductor, toneladas);
        }

        public string GetOfertasPedidoProducto(int idPedido, int idProducto, int idCalidad)
        {
            var ofertas = DetalleOferta.GetOfertasPedidoProducto(idPedido, idProducto, idCalidad);
            var json = JsonConvert.SerializeObject(ofertas);

            return json;

        }

        public string GetPedidosEnSeleccion()
        {
            var pedidos = Pedido.GetPedidosEnSeleccion();
            var json = JsonConvert.SerializeObject(pedidos);

            return json;

        }
        

        public bool ProductorOfertar(string json)
        {
            DetalleOferta detalle = JsonConvert.DeserializeObject<DetalleOferta>(json);

            return detalle.Ofertar();

        }

        public string GetDetallePedidoById(int idPedido, int idProducto, int idCalidad)
        {
            var detalles = DetallePedido.GetDetallePedidoById(idPedido, idProducto, idCalidad);

            string json = JsonConvert.SerializeObject(detalles);

            return json;
        }

        public bool AddDireccion(string json)
        {
            Direccion direccion = JsonConvert.DeserializeObject<Direccion>(json);

            if (direccion.AddDireccion())
                return true;
            else
                return false;
        }

        public bool RemoveDireccion(int idDireccion)
        {
            if (Direccion.RemoveDireccion(idDireccion))
                return true;
            else
                return false;
        }

        public string GetPedidosFinalizadosP(int idProductor)
        {
            return Productor.PedidosFinalizados(idProductor);
        }
        public string GetPedidosOfertados(int idProductor)
        {
            return Productor.PedidosOfertados(idProductor);
        }
        public string GetPedidosPendientes(int idProductor)
        {
            return Productor.PedidosPendientes(idProductor);
        }

        public string GetDirecciones(int clienteId)
        {
            var direcciones = Direccion.GetDireccionesCliente(clienteId);
            string json = JsonConvert.SerializeObject(direcciones);

            return json;
        }

        public bool ToggleHabilitado(int productoId, int productorId, int calidadId)
        {
            return ProductoOfrecido.ToggleHabilitado(productoId, productorId, calidadId);
        }
        public string GetCalidades()
        {
            var calidades = Calidad.GetCalidades();

            string jsonEnviar = JsonConvert.SerializeObject(calidades);

            return jsonEnviar;

        }

        public bool CrearPedido(string json)
        {
            Pedido pedido = JsonConvert.DeserializeObject<Pedido>(json);

            pedido.CrearPedido();

            return true;
        }

        public bool ResetPassword(string resetToken, string newPassword)
        {
            Usuario usuario = new Usuario();
            usuario.ResetToken = resetToken;
            usuario.Password = newPassword;
            if (usuario.ResetPassword())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public string GetResetToken(string email)
        {
            Usuario usuario = new Usuario();
            usuario.Email = email;

            if (usuario.GetByEmail())
            {
                return usuario.ResetToken;
            }
            else
            {
                return "Email incorrecto";
            }
        }

        public bool IsResetTokenValid(string token)
        {
            if (Utils.ResetTokenExists(token))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string IniciarSesion(string jsonRecibir)
        {
            Usuario userjson = JsonConvert.DeserializeObject<Usuario>(jsonRecibir);

            var TM = Usuario.IniciarSesion(userjson);

            string jsonEnviar = JsonConvert.SerializeObject(TM);

            return jsonEnviar;
        }

        public bool RegistrarUsuario(string json)
        {
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);

            switch (usuario.TipoUsuario)
            {
                case 1:
                    //Registrar cliente
                    Cliente cliente = JsonConvert.DeserializeObject<Cliente>(json);
                    return cliente.Registrar();
                case 2:
                    //Registrar productor
                    Productor productor = JsonConvert.DeserializeObject<Productor>(json);
                    return productor.Registrar();
                case 3:
                    //Registrar transportista
                    Transportista transportista = JsonConvert.DeserializeObject<Transportista>(json);
                    return transportista.Registrar();
                case 4:
                    //Registrar administrador
                    Administrador admin = JsonConvert.DeserializeObject<Administrador>(json);
                    return admin.Registrar();
                case 5:
                    //Registrar consultor
                    Consultor consultor = JsonConvert.DeserializeObject<Consultor>(json);
                    return consultor.Registrar();
                default:
                    return false;
            }
        }

        public bool CambiarContrasena(int idUsuario, string oldPassword, string newPassword)
        {
            return Usuario.CambiarContrasena(idUsuario, oldPassword, newPassword);
        }

        public int GetTotalDetalle(int idPedido, int idProducto)
        {
            int total = DetalleEscogido.GetTotalDetalle(idPedido, idProducto);
            return total;
        }
        public string GetSubasta(int idSubasta)
        {
            var subasta = Subasta.GetSubasta(idSubasta);

            string jsonEnviar = JsonConvert.SerializeObject(subasta);

            return jsonEnviar;
        }

        public string GetAllSubastas()
        {
            var subastas = Subasta.GetAllSubastas();

            string jsonEnviar = JsonConvert.SerializeObject(subastas);

            return jsonEnviar;
        }

        public string GetAllPedidos()
        {
            var pedidos = Pedido.GetAllPedidos();

            string jsonEnviar = JsonConvert.SerializeObject(pedidos);

            return jsonEnviar;
        }

        public bool IsTokenValid(int idUsuario, string token)
        {
            return Usuario.IsTokenValid(idUsuario, token);
        }

        public string GetDetallePedido(int id)
        {
            var detalle = DetallePedido.GetDetallePedido(id);

            string jsonEnviar = JsonConvert.SerializeObject(detalle);

            return jsonEnviar;
        }

        public string GetPedidoProductor(int idPedido, int idProductor)
        {
            var detalle = Pedido.GetPedido(idPedido, idProductor);

            string jsonEnviar = JsonConvert.SerializeObject(detalle);

            return jsonEnviar;
        }

        public string GetPedido(int idPedido)
        {
            var pedido = Pedido.GetPedido(idPedido);
            string jsonEnviar = JsonConvert.SerializeObject(pedido);

            return jsonEnviar;
        }

        public string GetPedidos(int idCliente)
        {
            var lista = Pedido.GetPedidos(idCliente);

            string jsonEnviar = JsonConvert.SerializeObject(lista);

            return jsonEnviar;
        }

        public string GetPedidosFinalizados(int idCliente)
        {
            var lista = Pedido.GetPedidosFinalizados(idCliente);

            string jsonEnviar = JsonConvert.SerializeObject(lista);

            return jsonEnviar;
        }
        public string GetPedidosActivos(int idCliente)
        {
            var lista = Pedido.GetPedidosActivos(idCliente);

            string jsonEnviar = JsonConvert.SerializeObject(lista);

            return jsonEnviar;
        }

        public string GetProducto(int idProducto)
        {
            Producto producto = new Producto(idProducto, "");

            producto.GetProductoById();

            string jsonEnviar = JsonConvert.SerializeObject(producto);


            return jsonEnviar;

        }
        public string GetProductos()
        {
            var lista = Producto.GetProductos();

            string jsonEnviar = JsonConvert.SerializeObject(lista);

            return jsonEnviar;
        }

        public string GetProductosOfrecidos(int idProductor)
        {
            var productos = Productor.GetProductosOfrecidos(idProductor);

            return JsonConvert.SerializeObject(productos);

        }

        public string GetProductosNoOfrecidos(int idProductor)
        {
            var productos = Productor.GetProductosNoOfrecidos(idProductor);

            return JsonConvert.SerializeObject(productos);

        }

        public bool AddProductoOfrecido(int idProductor, int idProducto, int idCalidad)
        {
            return Productor.AddProductoOfrecido(idProductor, idProducto, idCalidad);
        }
        public bool RemoveProductoOfrecido(int idProductor, int idProducto, int idCalidad)
        {
            return Productor.RemoveProductoOfrecido(idProductor, idProducto, idCalidad);
        }

    }
}
