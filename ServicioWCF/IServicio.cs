﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServicioWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicio" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicio
    {
        [OperationContract]
        string ReporteTransportistas(int rango);
        [OperationContract]
        bool HabilitarPago(int idPedido);

        [OperationContract]
        bool EnviarEncuesta(string encuestaJson);

        [OperationContract]
        string GetPreguntas();
        [OperationContract]
        bool RealizarPago(int idPedido);

        [OperationContract]
        bool ConfirmarRecepcion(int idPedido);

        [OperationContract]
        bool ConfirmarEntrega(int idPedido);

        [OperationContract]
        bool ConfirmarRetiro(int idPedido);

        [OperationContract]
        bool ToggleRetirar(int pedidoId, int productoId, int productorId, int calidadId);

        [OperationContract]
        bool InvitarPedido(int idPedido, string invitados);

        [OperationContract]
        string GetPosiblesProductores(int idPedido);

        [OperationContract]
        string GetAllTransportistas();

        [OperationContract]
        bool InvitarSubasta(int idSubasta, string invitados);

        [OperationContract]
        string GetInvitadosPedido(int idPedido);

        [OperationContract]
        string GetInvitadosSubasta(int idSubasta);

        [OperationContract]
        string GetReporte(DateTime fechaInicio, DateTime fechaTermino);

        [OperationContract]
        string GetProductosByCategoria(int idCategoria);

        [OperationContract]
        bool EscogerBid(int idSubasta, int idTransportista);

        [OperationContract]
        string GetBids(int idSubasta);

        [OperationContract]
        bool CancelarPedido(int idPedido);

        [OperationContract]
        bool QuitarOferta(int idPedido, int idProducto, int idCalidad, int idProductor);

        [OperationContract]
        bool FinalizarSubasta(int idSubasta);

        [OperationContract]
        string GetYourBid(int idSubasta, int idTransportista);

        [OperationContract]
        bool Bid(string bid);

        [OperationContract]
        string GetSubastasActivasWBid(int idTransportista);
        
        [OperationContract]
        string GetSubastasActivas();

        [OperationContract]
        string GetSubastasFinalizadasWBid(int idTransportista);
        
        [OperationContract]
        string GetSubastasFinalizadas();

        [OperationContract]
        bool ConfirmarPedido(int idPedido);

        [OperationContract]
        bool CompletarPedido(int idPedido);
            
        [OperationContract]
        bool EscogerOferta(int idPedido, int idProducto, int idCalidad, int idProductor, int toneladas);

        [OperationContract]
        string GetOfertasPedidoProducto(int idPedido, int idProducto, int idCalidad);

        [OperationContract]
        string GetPedidosEnSeleccion();

        [OperationContract]
        string GetPedidoProductor(int idPedido, int idProductor);

        [OperationContract]
        bool ProductorOfertar(string json);

        [OperationContract]
        string GetDetallePedidoById(int idPedido, int idProducto, int idCalidad);

        [OperationContract]
        bool AddDireccion(string json);

        [OperationContract]
        bool RemoveDireccion(int idDireccion);

        [OperationContract]
        string GetPedidosFinalizadosP(int idProductor);

        [OperationContract]
        string GetPedidosOfertados(int idProductor);

        [OperationContract]
        string GetPedidosPendientes(int idProductor);

        [OperationContract]
        string GetDirecciones(int clienteId);

        [OperationContract]
        bool ToggleHabilitado(int productoId, int productorId, int calidadId);

        [OperationContract]
        string GetCalidades();

        [OperationContract]
        bool CrearPedido(string json);

        [OperationContract]
        bool IsTokenValid(int id, string token);

        [OperationContract]
        string IniciarSesion(string json);

        [OperationContract]
        string GetProductos();

        [OperationContract]
        string GetPedidos(int from);

        [OperationContract]
        string GetPedidosFinalizados(int from);

        [OperationContract]
        string GetPedidosActivos(int from);

        [OperationContract]
        string GetPedido(int id);

        [OperationContract]
        string GetDetallePedido(int id);

        [OperationContract]
        string GetAllPedidos();

        [OperationContract]
        string GetProducto(int id_producto);

        [OperationContract]
        string GetAllSubastas();
        
        [OperationContract]
        string GetSubasta(int id_subasta);

        [OperationContract]
        int GetTotalDetalle(int id_pedido, int id_producto);

        [OperationContract]
        bool CambiarContrasena(int user_id, string contrasena_actual, string contrasena_nueva);

        [OperationContract]
        bool RegistrarUsuario(string json);

        [OperationContract]
        public string GetResetToken(string email);

        [OperationContract]
        public bool IsResetTokenValid(string token);

        [OperationContract]
        public bool ResetPassword(string reset_token, string password);

        [OperationContract]
        public string GetProductosOfrecidos(int idProductor);

        [OperationContract]
        public string GetProductosNoOfrecidos(int idProductor);

        [OperationContract]
        public bool AddProductoOfrecido(int idProductor, int idProducto, int idCalidad);

        [OperationContract]
        public bool RemoveProductoOfrecido(int idProductor, int idProducto, int idCalidad);

    }
}
